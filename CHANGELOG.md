# Changelog

- Resized move select and target select menus
- Added particle effects to the active combatant
- Added HUD indicator for the active combatant
- Balanced The Thief fight
- Added status effect icons on the HUD
- Updated animatic
- Lighting polish
- Updated tutorial
- Environment polish
- Updated incorrect move descriptions
- Fixed crash where time traveling to turn where combatants are stunned
- Fixed crash with Energy Siphon move
- Fixed volume sliders not working
- Fixed time travel button overlapping time tree nodes
- Fixed move indicators not resetting when time traveling
- Fixed turn nodes not always being selectable
