extends Spatial

### Entry point into the code base. Simply loads the animatic scene.

var Animatic = preload("res://Menus/Animatic/Animatic.tscn")

## Load the animatic scene.
func _load_animatic():
  var animatic = Animatic.instance()
  get_tree().get_root().call_deferred("add_child", animatic)

## Load the animatic when ready.
func _ready():
  _load_animatic()
  randomize()
  queue_free()
