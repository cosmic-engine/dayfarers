extends Node

### Singleton that tracks the game state, including
# victory flags. Also handles battle callbacks.

## Reference to the tree root.
onready var Root = get_tree().get_root() # Node

## Reference to the factories singleton.
onready var Factories = Root.get_node("Factories") # Node

## Reference to the encounters node list.
onready var encounters = get_node("Encounters") # Encounter[]

## Reference to the music singleton.
onready var Music = get_tree().get_root().get_node("Music") # Node

## Reference to the encounter details.
var encounter # Encounter

## Reference to the battle scene.
var battle # Battle

## Reference to the victory screen scene.
var victory_screen # PackedScene

## Reference to the defeat screen scene.
var defeat_screen # PackedScene

## Player progress flags for defeated bosses.
var victories = { # { String: Boolean }
  "liar": false,
  "cheater": false,
  "thief": false
}

## Instances a new battle from an encounter file.
func create_encounter(encounter_scene): # PackedScene
  encounter = encounter_scene.instance()
  encounters.add_child(encounter)

  battle = Factories.Battle.create_battle(encounter)
  add_child(battle)

  var level_select_menu = Root.get_node("LevelSelectMenu")
  Root.remove_child(level_select_menu)
  level_select_menu.queue_free()

  if OK != battle.connect("battle_won", self, "_on_battle_won"):
    assert(false)
  if OK != battle.connect("battle_lost", self, "_on_battle_lost"):
    assert(false)

## Callback to handle battle player win. Displays the victory screen.
func _on_battle_won():
  victories[encounter.encounter_name] = true
  victory_screen = Factories.Screen.create_victory_screen()
  Root.add_child(victory_screen)
  Music.play( Music.victory_intro )

## Callback to handle battle player loss. Displays the defeat screen.
func _on_battle_lost():
  defeat_screen = Factories.Screen.create_defeat_screen()
  Root.add_child(defeat_screen)
  Music.play( Music.defeat_stream)

## Terminates the existing battle and removes it from the tree.
func end_battle():
  encounters.remove_child(encounter)
  encounter.queue_free()
  remove_child(battle)
  battle.queue_free()
  if victory_screen != null:
    Root.remove_child(victory_screen)
    victory_screen.queue_free()
  if defeat_screen != null:
    Root.remove_child(defeat_screen)
    defeat_screen.queue_free()
