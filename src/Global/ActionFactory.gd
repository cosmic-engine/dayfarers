extends Node

### Factory singleton that is responsible for creating Action objects.

var DeadAction = preload("res://Battle/Actions/Variants/DeadAction/DeadAction.tscn") # Action
var RestAction = preload("res://Battle/Actions/Variants/RestAction/RestAction.tscn") # Action
var StunAction = preload("res://Battle/Actions/Variants/StunAction/StunAction.tscn") # Action
var AttackAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/AttackAction/AttackAction.tscn") # Action
var CounterAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/CounterAction/CounterAction.tscn") # Action
var HealAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/HealAction/HealAction.tscn") # Action
var RestoreAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/RestoreAction/RestoreAction.tscn") # Action
var DrainAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/DrainAction/DrainAction.tscn") # Action
var ApplyStatusEffectAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/ApplyStatusEffectAction/ApplyStatusEffectAction.tscn") # Action
var AttackAndApplyStatusEffectAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/AttackAndApplyStatusEffectAction/AttackAndApplyStatusEffectAction.tscn") # Action
var AttackAndApplyStatusEffectSelfAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/AttackAndApplyStatusEffectSelfAction/AttackAndApplyStatusEffectSelfAction.tscn") # Action
var ModifierAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/ModifierAction/ModifierAction.tscn") # Action
var BuffDebuffAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/BuffDebuffAction/BuffDebuffAction.tscn") # Action
var StatSwapAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/StatSwapAction/StatSwapAction.tscn") # Action
var WhatIfAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/WhatIfAction/WhatIfAction.tscn") # Action
var HeadsorTailsAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/HeadsorTailsAction/HeadsorTailsAction.tscn") # Action
var MineNowAction = preload("res://Battle/Actions/Variants/MoveAction/Variants/MineNowAction/MineNowAction.tscn") # Action

## Create an action where the combatant died.
func create_dead_action(combatant): # Combatant -> Action
  var action = DeadAction.instance()
  action.subject = combatant
  return action

## Create an action where the combatant rested, restoring AP by
# ap_restored amount.
func create_rest_action(combatant, ap_restored): # Combatant , Int -> Action
  var action = RestAction.instance()
  action.subject = combatant
  action.ap_restored = ap_restored
  return action

## Given a combatant, create an action where the combatant was stunned.
func create_stun_action(combatant): # Combatant -> Action
  var action = StunAction.instance()
  action.subject = combatant
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and dealing damage_dealt to each target.
func create_attack_action( # Combatant, Move, Combatant[], Int, Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  damage_dealt
 ):
  var action = AttackAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.damage_dealt = damage_dealt
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost, and countered the move successfully, reflecting it to
# the attacker, applying any status_applied originally applied to the
# combatant to the attacker.
func create_counter_action( # Combatant, Move, Combatant[], Int, Action, Boolean, Combatant, StatusEffect -> Action
  combatant,
  move,
  targets,
  ap_cost,
  countered_action,
  countered,
  attacker,
  status_applied
 ):
  var action = CounterAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.countered_action = countered_action
  action.countered = countered
  action.attacker = attacker
  action.status_applied = status_applied
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and adds amount to each target's HP.
func create_heal_action( # Combatant, Move, Combatant[], Int, Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  amount
):
  var action = HealAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.amount = amount
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and sets each target's HP or AP (depending on
# restore_health) to the restore_amount.
func create_restore_action( # Combatant, Move, Combatant[], Int, Int, Bool -> Action
  combatant,
  move,
  targets,
  ap_cost,
  restore_amount,
  restore_health
):
  var action = RestoreAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.restore_amount = restore_amount
  action.restore_health = restore_health
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and subtracts amount from each target's HP or AP
# (depending on restore_health) to the restore_amount.
func create_drain_action( # Combatant, Move, Combatant[], Int, Int, Bool -> Action
  combatant,
  move,
  targets,
  ap_cost,
  drain_amount,
  drain_health
):
  var action = DrainAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.drain_amount = drain_amount
  action.drain_health = drain_health
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and subtracts reduce_amount from each target's max HP
# or max AP (depending on reduce_health).
func create_heads_or_tails_action( # Combatant, Move, Combatant[], Int, Bool, Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  reduce_health,
  reduce_amount
):
  var action = HeadsorTailsAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.reduce_health = reduce_health
  action.reduce_amount = reduce_amount
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost and applied the status_applied status effect to the
# targets.
func create_apply_status_effect_action( # Combatant, Move, Combatant[], Int, StatusEffect -> Action
  combatant,
  move,
  targets,
  ap_cost,
  status_applied
):
  var action = ApplyStatusEffectAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.status_effect_applied = status_applied
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost, dealing damage_dealt damage to the targets, and
# applied the status_applied status effect to the targets.
func create_attack_and_apply_status_effect_action( # Combatant, Move, Combatant[], Int, Int, StatusEffect -> Action
  combatant,
  move,
  targets,
  ap_cost,
  damage_dealt,
  status_applied
  ):
  var action = AttackAndApplyStatusEffectAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.damage_dealt = damage_dealt
  action.status_effect_applied = status_applied
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost, dealing damage_dealt damage to combatant, and
# applied the status_applied status effect to combatant.
func create_attack_and_apply_status_effect_self_action( # Combatant, Move, Combatant[], Int, Int, StatusEffect -> Action
  combatant,
  move,
  targets,
  ap_cost,
  damage_dealt,
  status_applied
  ):
  var action = AttackAndApplyStatusEffectSelfAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.damage_dealt = damage_dealt
  action.status_effect_applied = status_applied
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost, buffing their attack, defense, or speed (depending on
# buff_type) by buff_amount for buff_duration.
func create_modifier_action( # Combatant, Move, Combatant[], Int, Enum, Int, Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  buff_type,
  buff_amount,
  buff_duration
):
  var action = ModifierAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.buff_type = buff_type
  action.buff_amount = buff_amount
  action.buff_duration = buff_duration
  return action

## Create an action where the combatant used a move on the given targets,
# costing ap_cost, buffing their attack, defense, or speed (depending on
# buff_type) by buff_amount for buff_duration, and debuffing their
# attack, defense, or speed (depending on
# debuff_type) by debuff_amount for debuff_duration.
func create_buff_debuff_action( # Combatant, Move, Combatant[], Int, Enum, Int, Int, Enum, Int, Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  buff_type,
  buff_amount,
  buff_duration,
  debuff_type,
  debuff_amount,
  debuff_duration
):
  var action = BuffDebuffAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.buff_type = buff_type
  action.buff_amount = buff_amount
  action.buff_duration = buff_duration
  action.debuff_type = debuff_type
  action.debuff_amount = debuff_amount
  action.debuff_duration = debuff_duration
  return action

## Create an action where the combatant used the special "what if?" move type.
func create_what_if_action( # Combatant, Move, Combatant[], Int -> Action
  combatant,
  move,
  targets,
  ap_cost
 ):
  var action = WhatIfAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  return action

## Create an action where the combatant used the special "stat swap" move type.
func create_stat_swap_action( # Combatant, Move, Combatant[], Int -> Action
  combatant,
  move,
  targets,
  ap_cost
 ):
  var action = StatSwapAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  return action
  
## Create an action where the combatant used the special "mine now" move type.
func create_mine_now_action( # Combatant, Move, Combatant[], Int -> Action
  combatant,
  move,
  targets,
  ap_cost,
  selected_move,
  damage_dealt,
  status_applied
 ):
  var action = MineNowAction.instance()
  action.subject = combatant
  action.move = move
  action.targets = targets
  action.ap_cost = ap_cost
  action.selected_move = selected_move
  action.damage_dealt = damage_dealt
  action.status_applied = status_applied
  return action
