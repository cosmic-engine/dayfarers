extends Node
### Creates a Turn object out of provided stats.

var Turn = preload("res://Battle/Interface/TimeTree/Turn.tscn") # Turn

## Makes a new turn with the appropriate stats and signal listeners.
func create_turn(stats, time_tree): # Stats, Time Tree -> Turn
  var turn = Turn.instance()
  turn.stats = stats

  if OK != turn.connect("on_select", time_tree, "_on_turn_selected"):
    assert(false, "Could not connect signal.")
  
  return turn
