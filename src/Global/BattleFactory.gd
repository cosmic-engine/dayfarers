extends Node

### Factory singleton responsible for creating battles.

var Battle = preload("res://Battle/Battle.tscn") # Battle

## Creates an instance of a battle given an encounter object.
func create_battle(encounter): # Encounter -> Battle
  var battle = Battle.instance()
  battle.player_party = encounter.player_party
  battle.enemy_party = encounter.enemy_party
  battle.arena = encounter.arena
  return battle
