extends Node

### Factory responsible for creating status effects.

var Healthy = preload("res://Battle/StatusEffects/Variants/Healthy/Healthy.tscn") # StatusEffect
var Dead = preload("res://Battle/StatusEffects/Variants/Dead/Dead.tscn") # StatusEffect
var Stun = preload("res://Battle/StatusEffects/Variants/Stun/Stun.tscn") # StatusEffect
var Confused = preload("res://Battle/StatusEffects/Variants/Confused/Confused.tscn") # StatusEffect
var Burn = preload("res://Battle/StatusEffects/Variants/Burn/Burn.tscn") # StatusEffect
var Bleed = preload("res://Battle/StatusEffects/Variants/Bleed/Bleed.tscn") # StatusEffect

## Create a new healthy status effect instance for the given combatant.
func create_healthy(combatant): # Combatant -> StatusEffect
  var status_effect = Healthy.instance()
  status_effect.combatant = combatant
  Temp.add_child(status_effect)
  return status_effect

## Create a new dead status effect instance for the given combatant.
func create_dead(combatant): # Combatant -> StatusEffect
  var status_effect = Dead.instance()
  status_effect.combatant = combatant
  Temp.add_child(status_effect)
  return status_effect

## Create a new stun status effect instance for the given combatant.
func create_stun(combatants, duration): # Combatant[], Int -> StatusEffect[]
  var status_effects = []
  for combatant in combatants:
    var status_effect = Stun.instance()
    status_effect.combatant = combatant
    status_effect.duration = duration
    Temp.add_child(status_effect)
    status_effects.append(status_effect)
  return status_effects

## Create a new confused status effect instance for the given combatant.
func create_confused(combatants, duration): # Combatant[], Int -> StatusEffect[]
  var status_effects = []
  for combatant in combatants:
    var status_effect = Confused.instance()
    status_effect.combatant = combatant
    status_effect.duration = duration
    Temp.add_child(status_effect)
    status_effects.append(status_effect)
  return status_effects

## Create a new burn status effect instance for the given combatant.
func create_burn(combatants, duration, damage): # Combatant[], Int -> StatusEffect[]
  var status_effects = []
  for combatant in combatants:
    var status_effect = Burn.instance()
    status_effect.combatant = combatant
    status_effect.duration = duration
    status_effect.damage = damage
    Temp.add_child(status_effect)
    status_effects.append(status_effect)
  return status_effects

## Create a new bleed status effect instance for the given combatant.
func create_bleed(combatants, duration, damage): # Combatant[], Int -> StatusEffect[]
  var status_effects = []
  for combatant in combatants:
    var status_effect = Bleed.instance()
    status_effect.combatant = combatant
    status_effect.duration = duration
    status_effect.damage = damage
    Temp.add_child(status_effect)
    status_effects.append(status_effect)
  return status_effects
