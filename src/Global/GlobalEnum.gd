extends Node

### Enums that need to be referenced by many scripts.

## Types of moves that exist in the combat system.
enum move_type { # Enum
  Attack,
  StatusEffectAttack,
  Modifier, 
  Restorative, 
  TimeTravel,
  Counter,
  Unique
  }

## Stats that can be buffed.
enum buff_type { # Enum
  Damage,
  Defense, 
  Speed 
  }
