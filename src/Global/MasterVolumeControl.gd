extends Node

## The music track volume slider value
var music_volume = 1.0 # Float

## The effects track volume slider value
var effects_volume = 0.5 # Float
