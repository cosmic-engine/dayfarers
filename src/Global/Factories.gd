extends Node

### Singleton reference to all factories in the project.
# Necessary workaround for a bug where Godot cannot
# cyclically reference classes.

## Battle factory singleton.
onready var Battle = $BattleFactory # BattleFactory

## Action factory singleton.
onready var Action = $ActionFactory # ActionFactory

## Turn factory singleton.
onready var Turn = $TurnFactory # TurnFactory

## Status effect factory singleton.
onready var StatusEffect = $StatusEffectFactory # StatusEffectFactory

## Stats factory singleton.
onready var Stats = $StatsFactory # StatsFactory

## Screen factory singleton.
onready var Screen = $ScreenFactory # ScreenFactory
