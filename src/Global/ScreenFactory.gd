extends Node

### Factory that can create victory or defeat screens.

var VictoryScreen = preload("res://Menus/GameOverMenu/BattleVictoryScreen.tscn") # PackedScene
var DefeatScreen = preload("res://Menus/GameOverMenu/BattleLossScreen.tscn") # PackedScene

## Create a victory screen instance.
func create_victory_screen(): # -> VictoryScreen
  var screen = VictoryScreen.instance()
  return screen

## Create a defeat screen instance.
func create_defeat_screen(): # -> DefeatScreen
  var screen = DefeatScreen.instance()
  return screen
