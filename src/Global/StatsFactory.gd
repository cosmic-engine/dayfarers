extends Node

### Factory responsible for creating stats and stat modifiers.

## Reference to the temp singleton.
onready var Temp = get_tree().get_root().get_node("Temp") # Node

var Stats = preload("res://Battle/Interface/TimeTree/Stats.tscn") # Stats
var StatModifiers = preload("res://Battle/Interface/TimeTree/StatModifiers.tscn") # StatModifiers
var CheatStats = preload("res://Battle/Interface/TimeTree/CheatStats.tscn") # CheatStats

## Create a stats object with the given parameters.
func create_stats( # Int, Int, Int, Int, Int, Int, StatModifiers, StatusEffect -> Stats
  max_hp,
  hp,
  max_ap,
  ap,
  defense,
  speed,
  stat_modifiers,
  status_effect,
  combatant
):
  if combatant.display_name == "The Cheater":
    var stats = CheatStats.instance()
    stats.max_hp = max_hp
    stats.hp = hp
    stats.max_ap = max_ap
    stats.ap = ap
    stats.defense = defense
    stats.speed = speed
    stats.stat_modifiers = stat_modifiers
    stats.status_effect = status_effect
    Temp.add_child(stats)
    return stats
  else:
    var stats = Stats.instance()
    stats.max_hp = max_hp
    stats.hp = hp
    stats.max_ap = max_ap
    stats.ap = ap
    stats.defense = defense
    stats.speed = speed
    stats.stat_modifiers = stat_modifiers
    stats.status_effect = status_effect
    Temp.add_child(stats)
    return stats

## Create a stat modifiers object with the given parameters.
func create_stat_modifiers( # Int, Int, Int, Int, Int, Int -> StatModifiers
  damage_modifier,
  damage_modifier_duration,
  defense_modifier,
  defense_modifier_duration,
  speed_modifier,
  speed_modifier_duration
):
  var stat_modifiers = StatModifiers.instance()
  stat_modifiers.damage_modifier = damage_modifier
  stat_modifiers.damage_modifier_duration = damage_modifier_duration
  stat_modifiers.defense_modifier = defense_modifier
  stat_modifiers.defense_modifier_duration = defense_modifier_duration
  stat_modifiers.speed_modifier = speed_modifier
  stat_modifiers.speed_modifier_duration = speed_modifier_duration
  Temp.add_child(stat_modifiers)
  return stat_modifiers
