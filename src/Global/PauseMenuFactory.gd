extends Node

### Factory that creates pause menu.

var PauseMenu = preload("res://Menus/PauseMenu/PauseMenu.tscn") # PackedScene

## Creates a new pause menu.
func create_pause_menu():
  var pause_menu = PauseMenu.instance()
  return pause_menu
