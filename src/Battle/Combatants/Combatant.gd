class_name Combatant
extends Spatial

### A single participant in the battle. A member of a party.
# Aggregates stats, status effects, buffs, and moves.
# Can select between moves and targets for those moves.
# Can execute the moves on the targets.

## Reference to the factories singleton.
onready var Factories = get_tree().get_root().get_node("Factories") # Node

## User facing display name.
export var display_name: String # String

## User facing character portrait image.
export var portrait: Texture # Texture

## Maximum health points allowed.
export var default_max_hp: int # Int

## Maximum action points allowed.
export var default_max_ap: int # Int

## Unmodified defense stat.
export var default_defense: int # Int

## Unmodified speed stat.
export var default_speed: int # Int

#warning-ignore:unused_signal
## Signal that emits once combatant has selected a move.
signal move_selected()

#warning-ignore:unused_signal
## Signal that emits once combatant has selected targets for the move.
signal targets_selected()

## Signal that emits once combatant stats have changed.
signal stats_changed()

## Signal that emits once combatant is being hovered on.
signal hover_start(combatant) # Combatant

## Signal that emits once combatant starts being hovered on.
signal hover_end(combatant) # Combatant

## Character model reference.
onready var model = $Model.get_child(0) # Model

## Character move indicator reference.
onready var move_indicator = $MoveIndicator # MoveIndicator

## Character stat indicator UI element reference.
onready var stat_indicator = $StatIndicator # StatIndicator

## Character target indicator UI element reference.
onready var target_indicator = $TargetIndicator # TargetIndicator

## Character particle system reference.
#onready var particles = $TimeTravelParticles # TimeTravelParticles

## Character stats object.
var stats setget _set_stats # Stats

## List of moves known by combatant.
var moves = [] # Move[]

## Party the combatant belongs to.
var party # Party

## Spawn point the combatant spawned at.
var spawn_point # Node

## Move the combatant has selected.
var selected_move # Move

## Targets the combatant has selected.
var selected_targets # Combatant[]

## Whether the combatant is being hovered over.
var hovering = false

## Whether the combatant is feigning death.
var play_dead = false

## Sets the combatant up when ready.
func _ready():
  _setup()

## Stores references to the members when the tree is entered.
func _enter_tree():
  party = $"../.."

## Prompts the combatant to select a move.
func select_move():
  assert(false)

## Interrupts move selection.
func cancel_select_move():
  return

## Prompts the combatant to select targets for the move.
func select_targets():
  assert(false)

## Makes combatant apply their status effects and use their move.
func take_turn():
  yield(stats.status_effect.apply(), "completed")

## Makes combatant use their move on their selected targets.
func use_move():
  return yield(selected_move.use(selected_targets), "completed")

## Initial combatant setup logic.
func _setup():
  _parse_moves()

## Parses moves known by the combatant.
func _parse_moves():
  for move in get_node("Moves").get_children():
    moves.append(move)

## Emits a signal once the selected move has changed.
func _on_move_selected(move): # Move
  selected_move = move
  emit_signal("move_selected")

## Emits a signal once the targets selected has changed.
func _on_targets_selected(targets): # Combatant[]
  selected_targets = targets
  emit_signal("targets_selected")

## Emits a signal once the stats has changed.
func _set_stats(new_stats): # Stats
  if stats != null:
    if play_dead:
      pass
    elif new_stats.is_dead() and !stats.is_dead():
      model.play_animation("ApplyDeath")
    elif !new_stats.is_dead():
      model.play_animation("ApplyIdle")
  stats = new_stats
  emit_signal("stats_changed")

## Emits a signal once the mouse has hovered over the combatant.
func _on_mouse_entered():
  emit_signal("hover_start", self)
  hovering = true

## Emits a signal once the mouse has stopped hovering over the combatant.
func _on_mouse_exited():
  emit_signal("hover_end", self)
  hovering = false
