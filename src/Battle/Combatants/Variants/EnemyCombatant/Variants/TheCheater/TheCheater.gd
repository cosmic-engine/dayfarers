class_name TheCheater
extends EnemyCombatant

### Combatant variant for the cheater boss.

## Picks a move from the combatant moveset at random.
func _get_random_move(): # -> Move
  if stats.hp <= -10:
    for move in moves:
      if move.name == "WhatIf":
        return move
  else:
    return moves[randi() % (len(moves) - 1)]

## Prompts the combatant to select targets for the move.
func select_targets():
  var selected_targets
  var available_targets = selected_move.get_available_targets()
  
  if selected_move.target_all:
    selected_targets = available_targets
  elif len(available_targets) == 1:
    selected_targets = available_targets
  else:
    selected_targets = [
      target_select_logic.select_target(available_targets)
    ]

  _on_targets_selected(selected_targets)
