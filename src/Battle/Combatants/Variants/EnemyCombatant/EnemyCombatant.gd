class_name EnemyCombatant
extends Combatant

### Combatant variant controlled by the computer.

## The logic object to determine targets for moves.
onready var target_select_logic = get_node("TargetSelectLogic").get_child(0) # TargetSelectLogic

## Prompts the combatant to select a move.
func select_move():
  var move = _get_random_move()
  _on_move_selected(move)

## Prompts the combatant to select targets for the move.
func select_targets():
  var selected_targets
  var available_targets = selected_move.get_available_targets()

  if selected_move.target_all:
    selected_targets = available_targets
  elif len(available_targets) == 1:
    selected_targets = available_targets
  else:
    selected_targets = [
      target_select_logic.select_target(available_targets)
    ]

  _on_targets_selected(selected_targets)

## Picks a move from the combatant moveset at random.
func _get_random_move(): # -> Move
  return moves[randi() % len(moves)]
