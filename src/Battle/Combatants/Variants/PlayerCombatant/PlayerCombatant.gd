class_name PlayerCombatant
extends Combatant

### Combatant variant controlled by the player.

## Initial combatant setup logic.
func _setup():
  ._setup()
  move_indicator.is_flipped = true

## Prompts the combatant to select a move.
func select_move():
  if OK != Game.battle.move_select_menu.connect("move_selected", self, "_on_move_selected"):
    assert(false, "Could not connect signal.")
  Game.battle.move_select_menu.open(self)

## Interrupts move selection.
func cancel_select_move():
  _close_move_select_menu()

## Prompts the combatant to select targets for the move.
func select_targets():
  if OK != Game.battle.target_select_menu.connect("targets_selected", self, "_on_targets_selected"):
    assert(false, "Could not connect signal.")
  Game.battle.target_select_menu.open(
    selected_move.get_available_targets(),
    selected_move.target_all
  )

## Emits a signal once the selected move has changed.
func _on_move_selected(move): # Move
  _close_move_select_menu()
  ._on_move_selected(move)

## Removes the move select menu from the UI.
func _close_move_select_menu():
  if Game.battle.move_select_menu.is_connected("move_selected", self, "_on_move_selected"):
    Game.battle.move_select_menu.disconnect("move_selected", self, "_on_move_selected")
  else:
    assert(false, "Signal was never connected.")
  Game.battle.move_select_menu.close()

## Emits a signal once the targets selected has changed.
func _on_targets_selected(targets): # Combatant[]
  if selected_move.is_time_travel_move:
    yield(selected_move.prompt_for_turn(targets), "completed")

  if selected_move.get("selected_turn") && selected_move.selected_turn == null:
    return
    
  ._on_targets_selected(targets)

## Removes the target select menu from the UI.
func close_target_select_menu():
  if Game.battle.target_select_menu.is_connected("targets_selected", self, "_on_targets_selected"):
    Game.battle.target_select_menu.disconnect("targets_selected", self, "_on_targets_selected")
  else:
    assert(false, "Signal was never connected.")
  Game.battle.target_select_menu.close()
