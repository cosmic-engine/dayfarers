extends Spatial

### The map where a given battle takes place. Responsible for
# spawning combatants onto the field.

onready var Music = get_tree().get_root().get_node("Music") # Node

## Player party object.
onready var player_party = $"../../PlayerParty".get_child(0) # Party

## Enemy party object.
onready var enemy_party = $"../../EnemyParty".get_child(0) # Party

## Player party spawn point spatial list.
onready var player_party_spawn_points = $"SpawnPoints/PlayerParty" # Node[]

## Enemy party spawn point spatial list.
onready var enemy_party_spawn_points = $"SpawnPoints/EnemyParty" # Node[]

## Music track number for this arena.
export var _music_choice: int # Int

## Spawns both parties in when ready.
func _ready():
  _spawn_enemy_party(enemy_party_spawn_points)
  _spawn_player_party(player_party_spawn_points)

## Returns the music track this arena plays.
func get_song(): # -> AudioStreamSample
  match _music_choice:
    1: return Music.rifts_intro
    2: return Music.cued_intro
    3: return Music.perpetual_intro

## Positions the player party combatants on their spawn points.
func _spawn_player_party(spawn_points): # Node[]
  for index in range(player_party.combatants.size()):
    var combatant = player_party.combatants[index]
    var spawn_point = spawn_points.get_child(index)
    _position_combatant(combatant, spawn_point)
    combatant.spawn_point = spawn_point
    spawn_point.combatant = combatant

## Positions the enemy party combatants on their spawn points.
func _spawn_enemy_party(spawn_points): # Node[]
  for index in range(enemy_party.combatants.size()):
    var combatant = enemy_party.combatants[index]
    var spawn_point = spawn_points.get_child(index)
    _position_combatant(combatant, spawn_point)
    combatant.spawn_point = spawn_point
    spawn_point.combatant = combatant

## Positionan individual combatant on a given spawn point.
func _position_combatant(combatant, spawn_point): # Combatant, Node
  combatant.transform.origin = spawn_point.transform.origin
  combatant.rotation_degrees = spawn_point.rotation_degrees
