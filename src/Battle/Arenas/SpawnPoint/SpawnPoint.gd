extends Spatial

### Spatial that determines where a combatant should spawn
# in an arena. Stores relationships to other spawn points.

## Reference to the spawn point path above this one.
export(NodePath) var above_spawn_point_path # NodePath

## Reference to the spawn point path below this one.
export(NodePath) var below_spawn_point_path # NodePath

## Reference to the spawn point path left of this one.
export(NodePath) var left_spawn_point_path # NodePath

## Reference to the spawn point path right of this one.
export(NodePath) var right_spawn_point_path # NodePath

## Reference to the combatant at this spawn point.
var combatant # Combatant

## Reference to the spawn point node above this one.
var above_spawn_point # Node

## Reference to the spawn point node below this one.
var below_spawn_point # Node

## Reference to the spawn point node left of this one.
var left_spawn_point # Node

## Reference to the spawn point node right of this one.
var right_spawn_point # Node

## Sets spawn point relationships on ready.
func _ready():
  above_spawn_point = get_node(above_spawn_point_path)
  below_spawn_point = get_node(below_spawn_point_path)
  left_spawn_point = get_node(left_spawn_point_path)
  right_spawn_point = get_node(right_spawn_point_path)
