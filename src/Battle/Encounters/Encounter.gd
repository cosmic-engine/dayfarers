class_name Encounter
extends Node

### Editor-driven template for the parameters of a battle,
# including both parties and the arena.

## The user facing title of the encounter.
export var encounter_name: String # String

## Reference to the player combatants.
onready var player_party = $"PlayerParty".get_child(0) # Party

## Reference to the enemy combatants.
onready var enemy_party = $"EnemyParty".get_child(0) # Party

## Reference to the arena scene.
onready var arena = $"Arena".get_child(0) # Arena
