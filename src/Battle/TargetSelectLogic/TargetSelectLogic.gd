class_name TargetSelectLogic
extends Node

### Visitor pattern for target selection for enemy combatants.
# Traverses the time tree by actions.

## Current depth the visitor has traversed in the time tree.
var depth = 0 # Int

## Determine the target to select out of the potential targets.
func select_target(_potential_targets): # Combatant[]
  assert(false)

## Visitor pattern for actions in the time tree.
func visit(action): # Action
  if action.get_class_type() == "DeadAction":
    _visit_dead_action(action)
  elif action.get_class_type() == "RestAction":
    _visit_rest_action(action)
  elif action.get_class_type() == "StunAction":
    _visit_stun_action(action)
  elif action.get_class_type() == "ApplyStatusEffectAction":
    _visit_apply_status_effect_action(action)
  elif action.get_class_type() == "AttackAndApplyStatusEffectAction":
    _visit_attack_and_apply_status_effect_action(action)
  elif action.get_class_type() == "AttackAndApplyStatusEffectSelfAction":
    _visit_attack_and_apply_status_effect_self_action(action)
  elif action.get_class_type() == "AttackAction":
    _visit_attack_action(action)
  elif action.get_class_type() == "HealAction":
    _visit_heal_action(action)
  elif action.get_class_type() == "ModifierAction":
    _visit_modifier_action(action)
  elif action.get_class_type() == "CounterAction":
    _visit_counter_action(action)
  elif action.get_class_type() == "StatSwapAction":
    _visit_stat_swap_action(action)
  elif action.get_class_type() == "WhatIfAction":
    _visit_what_if_action(action)
  elif action.get_class_type() == "BuffDebuffAction":
    _visit_buff_debuff_action(action)
  elif action.get_class_type() == "HeadsorTailsAction":
    _visit_heads_or_tails_action(action)
  elif action.get_class_type() == "RestoreAction":
    _visit_restore_action(action)
  elif action.get_class_type() == "DrainAction":
    _visit_drain_action(action)
  elif action.get_class_type() == "MineNowAction":
    _visit_mine_now_action(action)
  else:
    assert(false, "Undefined action type " + action.get_class_type())

# Visitor pattern for dead actions.
func _visit_dead_action(_action): # Action
  pass

# Visitor pattern for rest actions.
func _visit_rest_action(_action): # Action
  pass

# Visitor pattern for stun actions.
func _visit_stun_action(_action): # Action
  pass

# Visitor pattern for attack and apply status effect actions.
func _visit_attack_and_apply_status_effect_action(_action): # Action
  pass

# Visitor pattern for attack and apply status effect self actions.
func _visit_attack_and_apply_status_effect_self_action(_action): # Action
  pass

# Visitor pattern for apply status effect actions.
func _visit_apply_status_effect_action(_action): # Action
  pass

# Visitor pattern for attack actions.
func _visit_attack_action(_action): # Action
  pass

# Visitor pattern for heal actions.
func _visit_heal_action(_action): # Action
  pass

# Visitor pattern for modifier actions.
func _visit_modifier_action(_action): # Action
  pass
  
# Visitor pattern for buff debuff actions.
func _visit_buff_debuff_action(_action): # Action
  pass
  
# Visitor pattern for counter actions.
func _visit_counter_action(_action): # Action
  pass
  
# Visitor pattern for stat swap actions.
func _visit_stat_swap_action(_action): # Action
  pass
  
# Visitor pattern for what if actions.
func _visit_what_if_action(_action): # Action
  pass
  
# Visitor pattern for heads or tails actions.
func _visit_heads_or_tails_action(_action): # Action
  pass

# Visitor pattern for restore actions.
func _visit_restore_action(_action): # Action
  pass

# Visitor pattern for drain actions.
func _visit_drain_action(_action): # Action
  pass

# Visitor pattern for mine now actions.
func _visit_mine_now_action(_action): # Action
  pass
