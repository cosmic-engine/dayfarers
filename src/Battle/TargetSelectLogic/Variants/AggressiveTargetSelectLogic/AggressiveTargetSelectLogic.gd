class_name AggressiveTargetSelectLogic
extends TargetSelectLogic

### Target select logic variant that focuses primarily on
# selecting targets that are the greatest threat to the
# subject.

## The weight value for low response actions.
export var low_response: int # Int

## The weight value for medium response actions.
export var medium_response: int # Int

## The weight value for high response actions.
export var high_response: int # Int

## The combatant that has this logic.
onready var subject = $"../.." # Combatant

## The list of response weight values for each combatant.
var responses # { Combatant: Integer }

## Given a potential_targets list, return a single target.
func select_target(potential_targets): # Combatant[] -> Combatant
  responses = {}

  for combatant in potential_targets:
    responses[combatant] = 1

  var prev_action = Game.battle.time_tree.get_active_turn()._previous_action

  if prev_action == null:
    return _choose_random_target(potential_targets)

  prev_action.accept(self)

  return _calculate_target()

## Return a random target from the potential_targets.
func _choose_random_target(potential_targets): # Combatant[] -> Combatant
  var index = randi() % len(potential_targets)
  return potential_targets[index]

## Determine which target had the highest response weight value.
func _calculate_target(): # -> Combatant
  var total_response_count = _get_total_response_count()
  var response_percentages = _get_response_percentages(total_response_count)
  var selected_index = _get_selected_index()
  var selection = _get_selected_target(selected_index, response_percentages)
  return selection

## Return a reference to the target at the selected_index in the
# response_percentages.
func _get_selected_target(selected_index, response_percentages): # Int, Int[] -> Combatant
  for target in response_percentages.keys():
    if selected_index <= response_percentages[target] + 1:
      return target
  assert(false)

## Pick an index at random between 1 and 100.
func _get_selected_index(): # -> Int
  return randi() % 100 + 1

## Return an object with weight values per combatant.
func _get_response_percentages(total): #  Int -> # { Combatant: Integer }
  var response_percentages = {}
  for i in range(len(responses.values())):
    var key = responses.keys()[i]
    response_percentages[key] = (float(responses[key]) / total) * 100
    if i != 0:
      var prev_key = responses.keys()[i - 1]
      response_percentages[key] += response_percentages[prev_key]
  return response_percentages

## Returns the sum of responses.
func _get_total_response_count(): # -> Int
  var sum = 0

  for response in responses.values():
    sum += response

  return sum

## Visitor pattern for attack action.
func _visit_attack_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response

## Visitor pattern for heal action.
func _visit_heal_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += low_response

## Visitor pattern for modifier action.
func _visit_modifier_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += low_response

## Visitor pattern for buff debuff action.
func _visit_buff_debuff_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += low_response

## Visitor pattern for apply status effect action.
func _visit_apply_status_effect_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += low_response

## Visitor pattern for attack and apply status effect action.
func _visit_attack_and_apply_status_effect_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response

## Visitor pattern for attack and apply status effect self action.
func _visit_attack_and_apply_status_effect_self_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response
    
## Visitor pattern for counter action.
func _visit_counter_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response

## Visitor pattern for stats swap action.
func _visit_stat_swap_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response

## Visitor pattern for what if action.
func _visit_what_if_action(action): # Action
  if not action.subject in responses:
    return

  if subject in action.targets:
    responses[action.subject] += high_response
  else:
    responses[action.subject] += medium_response

## Visitor pattern for heads or tails action.
func _visit_heads_or_tails_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += low_response

## Visitor pattern for restore action.
func _visit_restore_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += low_response
  
## Visitor pattern for drain action.
func _visit_drain_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += medium_response

## Visitor pattern for mine now action.
func _visit_mine_now_action(action): # Action
  if not action.subject in responses:
    return

  responses[action.subject] += high_response
