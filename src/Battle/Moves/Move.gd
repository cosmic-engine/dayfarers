class_name Move
extends Node

### Chosen by the player, performed by a combatant, used on
# a target combatant.

onready var Game = get_tree().get_root().get_node("Game") # Node
onready var Factories = get_tree().get_root().get_node("Factories") # Node
onready var SFX = get_node("Sound Effect") # Node

## User facing name.
export var display_name: String # String

## User facing description.
export var description: String # String

## AP cost of the move.
export var ap_cost: int # Int

## Whether the move targets all combatants.
export var target_all: bool # Boolean

## Number denoting the priority of the move compared to other moves.
export(int, -3, 3) var move_priority # Int

## The type of move.
export(GlobalEnum.move_type) var move_type = GlobalEnum.move_type.Attack # Enum

## User facing icon representing the move.
export var icon: Texture # Texture

## The animation name to play on the user when executing the move.
export var move_animation: String # String

## The particle effect to play on the user when executing the move.
export var move_effect: String # String

## The animation to play on the targets after executing a move.
export var on_hit_animation: String # String

## The particle effect to play on the targets after executing a move.
export var on_hit_effect: String # String

## The combatant that used the move.
var user # Combatant

## Whether the move manipulates time.
var is_time_travel_move = false

## Grabs a reference to the user when entering the scene tree.
func _enter_tree():
  user = $"../.."

## Execute the move logic on the selected targets.
func use(_selected_targets): # Combatant[] -> Action
  assert(false)

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  assert(false)

## Returns a status effect to apply to the selected targets.
func get_status_applied(_selected_targets): # Combatant[] -> Null
  return null

## Play the animation tree of the move.
func animate(selected_targets):
  yield(get_tree(), "idle_frame")
  if user.model != null:
    user.model.play_animation(move_animation)
    SFX.play()
    user.model.play_effect(move_effect)
    yield(user.model.get_animation_player(), "animation_finished")
    var last_selected_target
    for selected_target in selected_targets:
      if !selected_target.play_dead:
        selected_target.model.play_animation(on_hit_animation)
        selected_target.model.play_effect(on_hit_effect)
        last_selected_target = selected_target
    if (
      last_selected_target != null &&
      last_selected_target.model.get_animation_player().has_animation(on_hit_animation)
    ):
      yield(last_selected_target.model.get_animation_player(), "animation_finished")
    # user.model.play_animation("ApplyIdle")
