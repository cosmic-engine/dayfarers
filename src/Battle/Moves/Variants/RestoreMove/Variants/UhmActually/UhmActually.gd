extends RestoreMove
### Restore 5 HP or 5 AP randomly.

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  if user.model != null && user.model.has_animation("attack"):
    user.model.play_animation("attack")
    yield(user.model.get_animation_player(), "animation_finished")
    user.model.play_animation("ApplyIdle")
    
  if randi() % 2: restore_health = true 

  return Factories.Action.create_restore_action(
    user, 
    self, 
    selected_targets, 
    ap_cost, 
    restore_amount,
    restore_health
  )
