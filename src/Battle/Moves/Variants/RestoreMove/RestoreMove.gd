class_name RestoreMove
extends Move

### Move that restores HP or AP to the target.

export var restore_amount: int # Int
export var accuracy: int # Int
export var restore_self: bool # Boolean
export var restore_health: bool # Boolean

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_restore_action(
    user, 
    self, 
    selected_targets, 
    ap_cost, 
    restore_amount,
    restore_health
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  if (restore_self):
    targets.append(user)
  else:
    for combatant in Game.battle.get_combatants():
      if !combatant.stats.is_dead():
        targets.append(combatant)
  
  return targets
