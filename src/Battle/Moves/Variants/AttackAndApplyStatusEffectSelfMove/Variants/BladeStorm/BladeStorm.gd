extends AttackAndApplyStatusEffectSelfMove

### Deals 5 damage, but stun user for 1 turn.

## The amound of time the status effect should remain applied on the targets.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect
  return Factories.StatusEffect.create_stun(selected_targets, duration)
