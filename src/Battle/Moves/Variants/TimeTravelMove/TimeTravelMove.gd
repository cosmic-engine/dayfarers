class_name TimeTravelMove
extends Move
### An abstract move type inherited by moves that affect turns
# other than the current one.

## The percentage change this move makes contact. 0 to 100.
export var accuracy: int # Int

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
