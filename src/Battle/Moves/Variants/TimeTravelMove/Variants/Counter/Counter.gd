class_name Counter
extends TimeTravelMove

### Move variant where the subject uses the special move counter, reflecting
# any actions where the subject was included in the list of targets back to the
# user of that move.

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")
  
  var enemy_action = find_enemy_action()
  var attacker = enemy_action.subject
  var countered = is_valid_enemy_move_action(selected_targets)
  var status_applied = null
  
  #check for counter success, if so modify previous actions and character stats
  if countered:
    if enemy_action.previous_turn != null:
      var target_restore_stats = enemy_action.previous_turn.stats[selected_targets[0]]
      revert_hp_and_status(selected_targets[0].stats, target_restore_stats)
    else:
      revert_default_hp_and_status(selected_targets)
    
    if (attacker.selected_move.move_type == GlobalEnum.move_type.StatusEffectAttack ):
      print("enemy is doing status effect attack")
      status_applied = attacker.selected_move.get_status_applied([attacker])
  
  return Factories.Action.create_counter_action(
    user,
    self,
    selected_targets,
    ap_cost,
    enemy_action,
    countered,
    attacker,
    status_applied
  )

## Sets the hp and status effect of the user back to the state they were in
# before the previous action was taken.
func revert_hp_and_status(sourcestats, targetrestorestats): # Stats, Stats
  sourcestats.hp = targetrestorestats.hp
  if (sourcestats.status_effect.display_name != "Healthy"):
    sourcestats.status_effect = targetrestorestats.status_effect

## Sets the default hp and default status effect of the user back to the state
# they were in before the previous action was taken.
func revert_default_hp_and_status(selected_targets): # Combatant[]
  selected_targets[0].stats.hp = selected_targets[0].stats.max_hp
  selected_targets[0].stats.status_effect = Factories.StatusEffect.create_healthly(selected_targets[0])

## Walks the time tree to find the previous action taken by the enemy.
func find_enemy_action(): # -> Action
  var action = Game.battle.time_tree.get_active_turn().get_previous_action()
  while (action.subject != user):
    if action.subject.party != user.party:
      return action
    else:
      action = action.previous_turn.get_previous_action()

## Whether the action the enemy took is valid to be countered.
func is_valid_enemy_move_action(selected_targets): # Combatant[] -> Boolean
  var action = Game.battle.time_tree.get_active_turn().get_previous_action()
  while (action.subject != user):
    if action.subject.party != user.party:
      return is_valid_move(action, selected_targets)
    else:
      action = action.previous_turn.get_previous_action()

## Whether the move the enemy used is valid to be countered.
func is_valid_move(action, selected_targets): # Action, Combatant[] -> Boolean
  if ((action.subject.selected_move.move_type == GlobalEnum.move_type.Attack || 
      action.subject.selected_move.move_type == GlobalEnum.move_type.StatusEffectAttack) && 
      action.subject.selected_targets[0] == selected_targets[0] && 
      len(action.subject.selected_targets) <= 1):
        print("Counter Succeeded")
        return true
  else:
    print("Counter Failed")
    return false

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  var party = Game.battle.player_party
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
