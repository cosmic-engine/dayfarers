extends ModifierMove

### Play dead for a turn and take no damage.

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  if user.model != null && user.model.has_animation("ApplyDeath"):
    user.model.play_animation("ApplyDeath")
    yield(user.model.get_animation_player(), "animation_finished")
    #user.model.play_animation("ApplyIdle")
    user.play_dead = true

  return Factories.Action.create_modifier_action(
    user,
    self,
    selected_targets,
    ap_cost,
    buff_type,
    buff_amount,
    buff_duration
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  if (buff_self):
    targets.append(user)
  else:
    var party = Game.battle.player_party
    for combatant in party.combatants:
      if !combatant.stats.is_dead():
        targets.append(combatant)
  
  return targets
