extends ModifierMove

### Reduces targets speed by 2.

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
