class_name ModifierMove
extends Move

### Move variant where a stat is buffed or debuffed.

## Whether the user of the move is the target.
export var buff_self = false # Boolean

## The type of buff to apply.
export(GlobalEnum.buff_type) var buff_type = GlobalEnum.buff_type.Damage # Enum

## The amount by which to buff the stat.
export var buff_amount: int # Int

## The number of turns the buff should last.
export var buff_duration: int # Int

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_modifier_action(
    user,
    self,
    selected_targets,
    ap_cost,
    buff_type,
    buff_amount,
    buff_duration
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  if (buff_self):
    targets.append(user)
  else:
    var party = Game.battle.player_party
    for combatant in party.combatants:
      if !combatant.stats.is_dead():
        targets.append(combatant)
  
  return targets


