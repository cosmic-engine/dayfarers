class_name AttackMove
extends Move

### Move variant where the subject uses an attack on the targets.

## Amount of HP to subtract from targets, before buffs/debuffs.
export var damage: int # Int

## The percentage change this move makes contact. 0 to 100.
export var accuracy: int # Int

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_attack_action(
    user,
    self,
    selected_targets,
    ap_cost,
    damage
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
