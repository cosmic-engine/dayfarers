extends AttackMove

### Throw a group of bombs in the air to deal 7 damage to everyone.

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = [user]
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
