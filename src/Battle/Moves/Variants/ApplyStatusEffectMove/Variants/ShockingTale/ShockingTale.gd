class_name ShockingTale
extends ApplyStatusEffectMove

### Tell a scary story that has a chance to stun the enemy with fear for a turn.

## How many turns to apply the status effect for.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_stun(selected_targets, duration)
