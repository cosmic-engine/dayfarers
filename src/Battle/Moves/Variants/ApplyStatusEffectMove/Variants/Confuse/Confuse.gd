class_name Confuse
extends ApplyStatusEffectMove

### Confuse and Enemy and causing their targetting to potentially be inaccurate.

## How many turns to apply the status effect for.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_confused(selected_targets, duration)
