class_name ApplyStatusEffectMove
extends Move

### Move variant where the user applies a status effect to
# the targets.

## Damage applied to the targets from the status effect.
var status_effect_damage # Int

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")
  yield(animate(selected_targets), "completed")

  return Factories.Action.create_apply_status_effect_action(
    user,
    self,
    selected_targets,
    ap_cost,
    get_status_applied(selected_targets)
  )

## Returns a status effect to apply to the selected targets.
func get_status_applied(_selected_targets): # Combatant[]
  assert(false)

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
