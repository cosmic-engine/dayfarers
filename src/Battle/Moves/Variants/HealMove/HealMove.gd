class_name HealMove
extends Move

### Move variant where the user restores HP to the target by
# siphoning resources from another turn.

## The amount by which to restore health.
export var healing: int # Int

## The chance that this move will succeed.
export var accuracy: int # Int

## Whether the user is also the target.
export var heal_self: bool # Boolean

## The turn to siphon HP from.
var selected_turn

## The stats of the target at that past turn.
var past_target_stats

## Mark this move as a time travel move on ready.
func _ready():
  is_time_travel_move = true

## Prompt the user to select a turn from the time tree, and
# siphon the HP from the target on that turn, giving the
# HP diff to the target on the current turn, instead.
func prompt_for_turn(selected_targets):
  var past_hp = -1
  while true:
    selected_turn = yield(Game.battle.time_tree.prompt_for_turn(), "completed")
    past_target_stats = selected_turn.stats[selected_targets[0]]
    past_hp = past_target_stats.hp

    if selected_turn == Game.battle.time_tree.get_active_turn():
      Game.battle.time_tree.message.text = "Cannot siphon from current turn!"
      Game.battle.time_tree.cancel_button.visible = false
      yield(get_tree().create_timer(3.0), "timeout")
    elif past_hp < healing:
      Game.battle.time_tree.message.text = "Cannot siphon from turn where {target} would fall below 0 HP!".format({
        "target": selected_targets[0].display_name
      })
      Game.battle.time_tree.cancel_button.visible = false
      yield(get_tree().create_timer(3.0), "timeout")
    elif selected_turn == null:
      return
    else:
      break

  Game.battle.time_tree.stop_prompt_for_turn()

  # TODO - Add feedback to the turn being modified

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  past_target_stats.hp -= healing
  selected_turn.update_display()

  yield(animate(selected_targets), "completed")
  
  selected_turn = null

  return Factories.Action.create_heal_action(
    user, 
    self, 
    selected_targets, 
    ap_cost, 
    healing
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  if (heal_self):
    targets.append(user)
  else:
    for combatant in Game.battle.get_combatants():
      if !combatant.stats.is_dead():
        targets.append(combatant)
  
  return targets
