extends AttackAndApplyStatusEffectMove

### Attack for 2 damage and apply burn for 2 turns.

## How many turns to apply the status effect for.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_burn(selected_targets, duration, status_effect_damage)
