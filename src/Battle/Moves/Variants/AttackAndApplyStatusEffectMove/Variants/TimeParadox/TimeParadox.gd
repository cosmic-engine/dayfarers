extends AttackAndApplyStatusEffectMove

### A risky attack with a chance to stun for 1 round and deal 3 damage.

## How many turns to apply the status effect for.
export var duration:int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_stun(selected_targets, duration)
