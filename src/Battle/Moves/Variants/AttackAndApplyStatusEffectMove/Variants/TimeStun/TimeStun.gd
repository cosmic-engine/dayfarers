extends AttackAndApplyStatusEffectMove
### Attack a single character and apply stun to them.

## How many turns to apply the status effect for.
export var duration:int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_stun(selected_targets, duration)
