extends AttackAndApplyStatusEffectMove

### Flash the entire enemy team, dealing 5 damage and possibly stunning each member of the team.

## How long to apply the status effect to the targeted combatants.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_stun(selected_targets, duration)
