extends AttackAndApplyStatusEffectMove

### Attack a single combatant for 11 damage and apply burn for 2 turns.

## How long to apply the status effect to the targeted combatants.
export var duration: int # Int

## Returns a status effect to apply to the selected targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_burn(selected_targets, duration, status_effect_damage)
