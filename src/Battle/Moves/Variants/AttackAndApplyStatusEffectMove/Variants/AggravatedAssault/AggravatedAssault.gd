extends AttackAndApplyStatusEffectMove

### Attack an enemy and cause bleed for 3 turns.

export var duration:int # Int

## Returns the status effect applied to the selected_targets.
func get_status_applied(selected_targets): # Combatant[] -> StatusEffect[]
  return Factories.StatusEffect.create_bleed(selected_targets, duration, status_effect_damage)
