class_name BuffDebuffMove
extends Move

### Move variant where the user buffs one of their own stats,
# and debuffs a different one of their own stats.

## Whether the buff or debuff is targeted at the user
export var buff_debuff_self = false # Boolean

## Damage, Defense, or Speed for buff
export(GlobalEnum.buff_type) var buff_type = GlobalEnum.buff_type.Damage # Enum

## How much to buff by
export(int, 0,6) var buff_amount # Int

## How long to buff for
export var buff_duration: int # Int

## Damage, Defense, or Speed for debuff
export(GlobalEnum.buff_type) var debuff_type = GlobalEnum.buff_type.Damage # Enum

## Damage, Defense, or Speed for debuff
export(int, -6,0) var debuff_amount # Int

## How long to debuff for
export var debuff_duration: int # Int

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_buff_debuff_action(
    user,
    self,
    selected_targets,
    ap_cost,
    buff_type,
    buff_amount,
    buff_duration,
    debuff_type,
    debuff_amount,
    debuff_duration
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  if (buff_debuff_self):
    targets.append(user)
  else:
    for combatant in Game.battle.get_combatants():
      if !combatant.stats.is_dead():
        targets.append(combatant)
  
  return targets
