class_name DrainMove
extends Move

### Move variant where stats are siphoned from an enemy combatant
# and restored to the subject.

export var drain_amount: int # Int
export var accuracy: int # Int
export var drain_health: bool # Boolean

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_drain_action(
    user, 
    self, 
    selected_targets, 
    ap_cost, 
    drain_amount,
    drain_health
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
