class_name HeadsorTails
extends Move

### Move variant. Randomly reduce an opponents max health or ap.

## The percentage change this move makes contact. 0 to 100.
export var accuracy: int # Int

## Whether to reduce max HP or max AP from the targets.
var reduce_hp = true # Boolean

## The amount by which to reduce HP or AP from the targets.
export var reduce_amount: float # float

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")
    
  if randi() % 2: reduce_hp = true 

  return Factories.Action.create_heads_or_tails_action(
    user, 
    self, 
    selected_targets, 
    ap_cost,
    reduce_hp,
    reduce_amount
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
