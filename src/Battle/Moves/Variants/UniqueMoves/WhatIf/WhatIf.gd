class_name WhatIf
extends Move
### Resets a combatant's health to a set value.

## The percentage change this move makes contact. 0 to 100.
export var accuracy: int # Int

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")

  yield(animate(selected_targets), "completed")

  return Factories.Action.create_what_if_action(
    user,
    self,
    selected_targets,
    ap_cost
  )

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  return [user]
