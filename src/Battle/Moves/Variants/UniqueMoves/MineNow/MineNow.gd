class_name MineNow
extends Move

### Take a random enemy's attack and use it against them.

## Execute the move logic on the selected targets.
func use(selected_targets): # Combatant[] -> Action
  yield(get_tree(), "idle_frame")
  
  yield(animate(selected_targets), "completed")

  var selected_move = _select_move(selected_targets)
  var damage_dealt = 0 if !selected_move.get("damage") else selected_move.damage
  var status_applied = (null if (selected_move.get_status_applied(selected_targets) == null) else selected_move.get_status_applied(selected_targets).duplicate())

  return Factories.Action.create_mine_now_action(
    user,
    self,
    selected_targets,
    ap_cost,
    selected_move,
    damage_dealt,
    status_applied
  )

## Determine which move to steal from the target.
func _select_move(selected_targets): #Combatant[] -> Move
  if (_isFreshRound() == false):
    return _get_previously_selected_move()
  else: 
    var movelist = []
    for move in selected_targets[0].moves:
      if move.move_type == GlobalEnum.move_type.Attack || move.move_type == GlobalEnum.move_type.StatusEffectAttack:
        movelist.append(move)
    return movelist[randi() % movelist.size()]

## Whether this round is valid to steal a move from.
func _isFreshRound(): # -> Boolean
  var cur_turn = Game.battle.time_tree.get_active_turn()
  var cur_action = null
  while (!cur_turn.is_round_start()):
    cur_action = cur_turn.get_previous_action()
    cur_turn = cur_action.previous_turn
  
  var forward_action = cur_turn.get_next_actions()[0]
  var forward_turn = forward_action.next_turn
  while(!forward_turn.is_round_start()):
    if(forward_turn.get_next_actions().size() > 0 && forward_turn.get_next_actions()[0] != null):
      forward_action = forward_turn.get_next_actions()[0]
      forward_turn = forward_action.next_turn
    else:
      return true
  return false

## Return a reference to the move selected last.
func _get_previously_selected_move(): # -> Move
  var cur_turn = Game.battle.time_tree.get_active_turn()
  var cur_action = null
  while (!cur_turn.is_round_start()):
    cur_action = cur_turn.get_previous_action()
    cur_turn = cur_action.previous_turn
  
  var forward_action = cur_turn.get_next_actions()[0]
  var forward_turn = forward_action.next_turn
  while (forward_action.subject != user):
    forward_action = forward_turn.get_next_actions()[0]
    forward_turn = forward_action.next_turn
  return forward_action.selected_move

## Returns a list of valid targets for this move.
func get_available_targets(): # -> Combatant[]
  var targets = []
  
  var party = Game.battle.get_opposing_party(user)
  for combatant in party.combatants:
    if !combatant.stats.is_dead():
      targets.append(combatant)
  
  return targets
