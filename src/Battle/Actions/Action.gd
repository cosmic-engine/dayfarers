class_name Action
extends Node2D

### The representation of the diff between two turns, including
# the subject of the diff, what occurred, and how to restore to
# that state.

onready var Game = get_tree().get_root().get_node("Game") # Node
onready var Factories = get_tree().get_root().get_node("Factories") # Node

## The combatant that performed the action.
var subject # Combatant

## The turn immediately preceding the action.
var previous_turn # Turn

## The list of turns immediately following the action.
var next_turn # Turn[]

func _ready():
  _draw_info()

## Determines the new stats of all combatants after this action is
# performed.
func create_all_stats(): # -> Stats[]
  var stats = {}
  for combatant in Game.battle.get_combatants():
    stats[combatant] = create_stats(combatant)
  return stats

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(_combatant): # Combatant -> Stats
  assert(false)

## Reverts the state of the subject to that when this action was performed.
func restore():
  assert(false)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  assert(false)

## Accepts time tree traversing visitors.
func accept(visitor): # Action
  visitor.visit(self)
  if !previous_turn.is_round_start():
    previous_turn._previous_action.accept(visitor)

## Whether this action is equivalent to other.
func equals(_other): # Action -> Boolean
  assert(false)

## Returns the class name.
func get_class_type(): # -> String
  return get_script().get_path().get_file().split(".", true, 1)[0]

## Updates the UI with action information.
func _draw_info():
  assert(false)

## Creates a duplicate stats object identical to the one
# the given combatant currently has.
func _create_default_stats(combatant): # Combatant -> Stats
  return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      combatant.stats.status_effect.current(),
      combatant
    )
