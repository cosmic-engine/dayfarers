class_name StunAction
extends Action

### Action where the combatant is stunned.

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  # if combatant is the subject
  if combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Reverts the state of the subject to that when this action was performed.
func restore():
  # TODO - fix hardcoded value
  var status_effects_restore = Factories.StatusEffect.create_stun([subject], 1)
  for status_effect in status_effects_restore:
    if status_effect.combatant == subject:
       subject.stats.status_effect = status_effect
  

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return "{combatant} was stunned and couldn't move.".format({
    "combatant": subject.display_name
  })

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject
  )

## Updates the UI with action information.
func _draw_info():
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var _targets = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var _move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var _move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  effect_label.text = "Was stunned!"
