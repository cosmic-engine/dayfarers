class_name DeadAction
extends Action

### Action variant where the subject cannot perform any additional actions.

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  return _create_default_stats(combatant)

## Reverts the state of the subject to that when this action was performed.
func restore():
  subject.stats.status_effect = Factories.StatusEffect.create_dead(subject)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return "{subject} is dead".format({
    "subject": subject.display_name
  })

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject
  )

## Updates the UI with action information.
func _draw_info():
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var _targets = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var _move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var _move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  effect_label.text = "was dead!"
