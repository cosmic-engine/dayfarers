class_name DrainAction
extends MoveAction

### Action variant where stats are siphoned from an enemy combatant
# and restored to the subject.

## Value to decrement from the target turn.
var drain_amount # Int

## Whether HP is the stat to drain, or AP.
var drain_health # Boolean

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  get_drain_amount(combatant)
  
  # if combatant is the subject and they heal themself
  if combatant == subject && combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is the subject
  elif combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      (combatant.stats.hp + drain_amount if drain_health else combatant.stats.hp),
      combatant.stats.max_ap,
      (combatant.stats.ap + drain_amount if !drain_health else combatant.stats.ap) - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is a target
  elif combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      (combatant.stats.hp - drain_amount if drain_health else combatant.stats.hp),
      combatant.stats.max_ap,
      (combatant.stats.ap - drain_amount if !drain_health else combatant.stats.ap),
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      combatant.stats.status_effect.current(),
      combatant
    )
  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Returns the damage value that should be applied to the combatant
# when draining.
func get_drain_amount(combatant):
  if (drain_health):
    if (drain_amount > combatant.stats.hp):
      drain_amount = combatant.stats.hp
  else:
    if (drain_amount > combatant.stats.ap):
      drain_amount = combatant.stats.ap

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return ("{combatant} has drained {drainamount}{stat} from {target}".format({
    "combatant": subject.display_name,
    "drainamount": drain_amount,
    "stat": ("HP" if drain_health else "AP"),
    "target": targets[0].display_name
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.drain_amount == drain_amount and
    other.drain_health == drain_health
  )

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = "Drained {drainamount}{stat}!".format({
    "target": targets[0].display_name,
    "drainamount": drain_amount,
    "stat": ("HP" if drain_health else "AP")
  })
