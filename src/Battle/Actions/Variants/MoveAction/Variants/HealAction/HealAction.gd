class_name HealAction
extends MoveAction

### Action variant where the user restores HP to the target by
# siphoning resources from another turn.

## The amount of HP to restore to the targets.
var amount # Int

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  var final_hp = combatant.stats.hp + amount
  if (final_hp > combatant.stats.max_hp):
    final_hp = combatant.stats.max_hp
  # if combatant is the subject and they heal themself
  if combatant == subject && combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      final_hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is the subject
  elif combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is a target
  elif combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      final_hp,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      combatant.stats.status_effect.current(),
      combatant
    )
  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return ("{combatant} has healed {target} for {amount}HP".format({
    "combatant": subject.display_name,
    "target": targets[0].display_name,
    "amount": amount
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.amount == amount
  )

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = "Healed for {amount}HP".format({
    "amount": amount
  })
