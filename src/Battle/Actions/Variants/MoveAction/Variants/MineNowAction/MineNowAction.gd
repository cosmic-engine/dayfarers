class_name MineNowAction
extends MoveAction

### Take a random enemy's attack and use it against them.

## The amount of HP to remove from the targets, before buffs and debuffs. 
var damage_dealt # Int

## The status effect that was applied as a result of the countered action.
var status_applied # StatusEffect

## The move stolen from the target, to be used on the target.
var selected_move # Move

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats 
  var hp_after_damage = _calculate_hp_after_damage_dealt(combatant)
  
  if combatant == subject && combatant in targets && hp_after_damage == 0:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      Factories.StatusEffect.create_dead(combatant),
      combatant
    )

  elif combatant == subject && combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      (combatant.stats.status_effect.next() if status_applied == null else _get_target_status_effect(combatant)),
      combatant
    )

  elif combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )

  elif combatant in targets && hp_after_damage == 0:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      Factories.StatusEffect.create_dead(combatant),
      combatant
    )

  elif combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      (combatant.stats.status_effect.current() if status_applied == null else _get_target_status_effect(combatant)),
      combatant
    )

  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Returns the status effect refernce the given combatant has.
func _get_target_status_effect(combatant): # Combatant -> StatusEffect
  for status_effect in status_applied:
    if status_effect.combatant == combatant:
      return status_effect

## Determines the real damage to apply to the combatant, given their
# buffs to defense and the user's buffs to attack.
func _calculate_true_damage(combatant): # Combatant -> Int
  var applied_damage_dealt = damage_dealt # + subject.stats.stat_modifiers.damage_modifier
  
  # make damage calculation account for defense
  # TODO MAKE IT SO REAL DAMAGE IS SHOWN WHEN TARGET HAS REDUCED DEFENSE
  if (applied_damage_dealt <= (combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier)):
    applied_damage_dealt = 0
  else:
    applied_damage_dealt += subject.stats.stat_modifiers.damage_modifier
    applied_damage_dealt -= combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier
  return applied_damage_dealt

## Determines the new hp value the combatant should have after this
# action is executed.
func _calculate_hp_after_damage_dealt(combatant): # Combatant -> Int
  var applied_damage_dealt = _calculate_true_damage(combatant)
  
  var new_hp = combatant.stats.hp - applied_damage_dealt

  if new_hp < 0:
    new_hp = 0

  return new_hp

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return ("{combatant} stole {move} and dealt {damage} damage{applied_status_effect} to {target}.".format({
    "combatant": subject.display_name,
    "move": selected_move.display_name,
    "damage": _calculate_true_damage(targets[0]),
    "applied_status_effect": ("" if status_applied == null else " and applied " + status_applied[0].display_name),
    "target": targets[0].display_name
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.damage_dealt == damage_dealt and
    (
      (other.status_applied == null and status_applied == null) or 
      (other.status_applied != null and status_applied != null) and (other.status_applied[0].display_name == status_applied[0].display_name)
    ) and
    other.selected_move == selected_move
  )

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = "{applied_status_effect}{divider}{damage}!".format({
    "applied_status_effect" : ("" if status_applied == null else status_applied[0].display_name),
    "divider" : (" / " if  damage_dealt != 0 && status_applied != null else ""),
    "damage" : ("" if damage_dealt == 0 else str(_calculate_true_damage(targets[0])) + " Damage Dealt")
  })
