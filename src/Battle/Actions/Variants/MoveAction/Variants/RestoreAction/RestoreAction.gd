class_name RestoreAction
extends MoveAction

### Move that restores HP or AP to the target.

## The value to be restored to the target stat.
var restore_amount # Int

## Whether to restore HP or AP.
var restore_health # Boolean

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  var final_hp = combatant.stats.hp
  var final_ap = combatant.stats.ap
  if restore_health:
    final_hp = combatant.stats.hp + restore_amount
  else:
    final_ap = combatant.stats.ap + restore_amount
  
  if (final_hp > combatant.stats.max_hp):
    final_hp = combatant.stats.max_hp
    
  if (final_ap > combatant.stats.max_ap):
    final_ap = combatant.stats.max_ap  
  
  # if combatant is the subject and they heal themself
  if combatant == subject && combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      final_hp,
      combatant.stats.max_ap,
      final_ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is the subject
  elif combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )
  # if combatant is a target
  elif combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      final_hp,
      combatant.stats.max_ap,
      final_ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      combatant.stats.status_effect.current(),
      combatant
    )
  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return ("{combatant} has restored {target} {restoretype} by {restoreamount}".format({
    "combatant": subject.display_name,
    "target": (targets[0].display_name + "'s" if subject.display_name != targets[0].display_name else "their own"),
    "restoretype": ("HP" if restore_health else "AP"),
    "restoreamount": restore_amount
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.restore_amount == restore_amount and
    other.restore_health == restore_health
  )

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = ("Restored {restoretype} by {restoreamount}!".format({
    "restoretype": ("HP" if restore_health else "AP"),
    "restoreamount": restore_amount
  }))
