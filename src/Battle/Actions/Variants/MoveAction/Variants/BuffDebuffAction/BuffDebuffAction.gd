class_name BuffDebuffAction
extends MoveAction

### Action variant where the user buffs one of their own stats,
# and debuffs a different one of their own stats.

var buff_type # Enum
var buff_amount # Int
var buff_duration # Int

var debuff_type # Enum
var debuff_amount # Int
var debuff_duration # Int

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  if combatant == subject && combatant in targets:
    var stats = Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
      )
    buff_combatant_stats(stats)
    debuff_combatant_stats(stats)
    return stats
  elif combatant == subject:
    var stats = Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
      )
    debuff_combatant_stats(stats)
    return stats
  elif combatant in targets:
    var stats = Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      combatant.stats.status_effect.current(),
      combatant
    )
    buff_combatant_stats(stats)
    return stats
  else:
    return _create_default_stats(combatant)

## Applies buff amount and buff duration to combatant stats, according to buff type.
func buff_combatant_stats(stats): # Stats
  match buff_type:
      GlobalEnum.buff_type.Damage:
        stats.stat_modifiers.damage_modifier = buff_amount
        stats.stat_modifiers.damage_modifier_duration = buff_duration
      GlobalEnum.buff_type.Defense:
        stats.stat_modifiers.defense_modifier = buff_amount
        stats.stat_modifiers.defense_modifier_duration = buff_duration
      GlobalEnum.buff_type.Speed:
        stats.stat_modifiers.speed_modifier = buff_amount
        stats.stat_modifiers.speed_modifier_duration = buff_duration
        
## Applies debuff amount and debuff duration to combatant stats, according to debuff type.
func debuff_combatant_stats(stats): # Stats
  match debuff_type:
    GlobalEnum.buff_type.Damage:
        stats.stat_modifiers.damage_modifier = debuff_amount
        stats.stat_modifiers.damage_modifier_duration = debuff_duration
    GlobalEnum.buff_type.Defense:
      stats.stat_modifiers.defense_modifier = debuff_amount
      stats.stat_modifiers.defense_modifier_duration = debuff_duration
    GlobalEnum.buff_type.Speed:
      stats.stat_modifiers.speed_modifier = debuff_amount
      stats.stat_modifiers.speed_modifier_duration = debuff_duration

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  var buffEnumKeys = GlobalEnum.buff_type.keys()
  return ("{combatant} buffed {target} {bufftype} by {buffamount} and debuffed their own {debufftype} by {debuffamount}".format({
    "combatant": subject.display_name,
    "buffdebuff": ("buffed" if buff_amount > 0 else "debuffed"),
    "target": (targets[0].display_name + "'s" if subject.display_name != targets[0].display_name else "their own"),
    "bufftype": buffEnumKeys[buff_type],
    "buffamount": buff_amount,
    "debufftype": buffEnumKeys[debuff_type],
    "debuffamount": debuff_amount,
    "buffduration": (str(buff_duration) + " turn" if buff_duration == 1 else str(buff_duration) + " turns"),
    "debuffduration": (str(debuff_duration) + " turn" if debuff_duration == 1 else str(debuff_duration) + " turns")
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.buff_type == buff_type and
    other.buff_amount == buff_amount and
    other.buff_duration == buff_duration and
    other.debuff_type == debuff_type and
    other.debuff_amount == debuff_amount and
    other.debuff_duration == debuff_duration
  )

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = "Gave up some power!"
