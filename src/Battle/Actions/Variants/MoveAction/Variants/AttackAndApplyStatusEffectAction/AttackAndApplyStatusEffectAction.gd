class_name AttackAndApplyStatusEffectAction
extends MoveAction

### Action variant where the subject uses an attack move on the targets,
# and applies status effects to them as well.

## The damage the move dealt to the targets.
var damage_dealt # Int

## The status effects the move applied to the targets.
var status_effect_applied # StatusEffect

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  var hp_after_damage = _calculate_hp_after_damage_dealt(combatant)

  if combatant == subject && combatant in targets && hp_after_damage == 0:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      Factories.StatusEffect.create_dead(combatant),
      combatant
    )

  elif combatant == subject && combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      _get_target_status_effect(combatant),
      combatant
    )

  elif combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )

  elif combatant in targets && hp_after_damage == 0:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      Factories.StatusEffect.create_dead(combatant),
      combatant
    )

  elif combatant in targets:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      _get_target_status_effect(combatant),
      combatant
    )

  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  if (move.target_all && subject in targets):
    return ("{combatant} used {move}, dealing {damage} DMG, applying {status_effect_applied} to everyone".format({
      "combatant": subject.display_name,
      "move": move.display_name,
      "damage": damage_dealt,
      "status_effect_applied": status_effect_applied[0].display_name
    }))
  elif (move.target_all):
    return ("{combatant} used {move}, dealing {damage} DMG, applying {status_effect_applied} to the {target}".format({
      "combatant": subject.display_name,
      "move": move.display_name,
      "damage": damage_dealt,
      "target": targets[0].party.display_name,
      "status_effect_applied": status_effect_applied[0].display_name
    }))
  else:
    return ("{combatant} used {move}, dealing {damage} DMG to {target}, applying {status_effect_applied}".format({
      "combatant": subject.display_name,
      "move": move.display_name,
      "damage": _calculate_true_damage(targets[0]),
      "target": targets[0].display_name,
      "status_effect_applied": status_effect_applied[0].display_name
    }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.damage_dealt == damage_dealt and
    other.status_effect_applied[0].display_name == status_effect_applied[0].display_name
  )

## Returns the status effect refernce the given combatant has.
func _get_target_status_effect(combatant): # Combatant -> StatusEffect
  for status_effect in status_effect_applied:
    if status_effect.combatant == combatant:
      return status_effect

## Determines the real damage to apply to the combatant, given their
# buffs to defense and the user's buffs to attack.
func _calculate_true_damage(combatant): # Combatant -> Int
  var applied_damage_dealt = damage_dealt # + subject.stats.stat_modifiers.damage_modifier
  
  # make damage calculation account for defense
  # TODO MAKE IT SO REAL DAMAGE IS SHOWN WHEN TARGET HAS REDUCED DEFENSE
  if (applied_damage_dealt <= (combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier)):
    applied_damage_dealt = 0
  else:
    applied_damage_dealt += subject.stats.stat_modifiers.damage_modifier
    applied_damage_dealt -= combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier
  return applied_damage_dealt

## Determines the new hp value the combatant should have after this
# action is executed.
func _calculate_hp_after_damage_dealt(combatant): # Combatant -> Int
  var applied_damage_dealt = _calculate_true_damage(combatant)
  
  var new_hp = combatant.stats.hp - applied_damage_dealt

  if new_hp < 0:
    new_hp = 0

  return new_hp

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  for target in targets:
    var target_portrait = TargetPortrait.instance()
    target_portrait.texture = target.portrait
    targets_box.add_child(target_portrait)
  effect_label.text = "{effect} / {damage} damage dealt!".format({
    "effect": status_effect_applied[0].display_name,
    "damage":  (damage_dealt if targets.size() > 1 else _calculate_true_damage(targets[0]))
  })
