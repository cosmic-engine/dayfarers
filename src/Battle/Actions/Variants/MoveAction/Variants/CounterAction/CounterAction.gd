class_name CounterAction
extends MoveAction

### Action variant where the subject uses the special move counter, reflecting
# any actions where the subject was included in the list of targets back to the
# user of that move.

## The action to revert to the subject.
var countered_action # Action

## The combatant that attacked on the countered action.
var attacker # Combatant

## Whether the counter succeeded.
var countered # Boolean

## The status effect that was applied as a result of the countered action.
var status_applied # StatusEffect

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  var hp_after_damage = (combatant.stats.hp if countered == false else _calculate_hp_after_damage_dealt(combatant))
  
  if combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      combatant.stats.ap - ap_cost,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(),
      combatant
    )

  elif combatant == attacker && countered:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      hp_after_damage,
      combatant.stats.max_ap,
      combatant.stats.ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.current(),
      (combatant.stats.status_effect.current() if status_applied == null else _get_target_status_effect(combatant)),
      combatant
    )

  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  if (countered):
    return "{combatant} countered {attacker}'s attack, and reapplied it to them".format({
    "combatant": subject.display_name,
    "attacker": attacker.display_name
  })
  else:
    return "{combatant} missed their counter!".format({
    "combatant": subject.display_name
    })
  
## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.move == move and
    other.targets == targets and
    other.ap_cost == ap_cost and
    other.countered_action == countered_action and
    other.attacker == attacker and
    other.countered == countered and
    other.status_applied[0].display_name == status_applied[0].display_name
  )
  
## Returns the status effect refernce the given combatant has.
func _get_target_status_effect(combatant): # Combatant -> StatusEffect
  for status_effect in status_applied:
    if status_effect.combatant == combatant:
      return status_effect

## Determines the real damage to apply to the combatant, given their
# buffs to defense and the user's buffs to attack.
func _calculate_true_damage(combatant): # Combatant -> Int
  var applied_damage_dealt = countered_action.damage_dealt # + subject.stats.stat_modifiers.damage_modifier
  
  # make damage calculation account for defense
  # TODO MAKE IT SO REAL DAMAGE IS SHOWN WHEN TARGET HAS REDUCED DEFENSE
  if (applied_damage_dealt <= (combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier)):
    applied_damage_dealt = 0
  else:
    applied_damage_dealt += subject.stats.stat_modifiers.damage_modifier
    applied_damage_dealt -= combatant.stats.defense + combatant.stats.stat_modifiers.defense_modifier
  return applied_damage_dealt

## Determines the new hp value the combatant should have after this
# action is executed.
func _calculate_hp_after_damage_dealt(combatant): # Combatant -> Int
  var applied_damage_dealt = _calculate_true_damage(combatant)
  
  var new_hp = combatant.stats.hp - applied_damage_dealt

  if new_hp < 0:
    new_hp = 0

  return new_hp

## Updates the UI with action information.
func _draw_info():
  var TargetPortrait = load("res://Battle/Actions/Assets/TargetPortrait.tscn")
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var targets_box = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  move_name_label.text = move.display_name
  move_icon.texture = move.icon
  var target_portrait = TargetPortrait.instance()
  target_portrait.texture = attacker.portrait
  targets_box.add_child(target_portrait)
  if (countered):
    effect_label.text = "{applied_status_effect}{divider}{damage}!".format({
    "applied_status_effect" : ("" if status_applied == null else status_applied[0].display_name),
    "divider" : ("" if !countered_action.get("damage_dealt") || countered_action.damage_dealt == 0 && status_applied != null else " / "),
    "damage" : ("" if !countered_action.get("damage_dealt") || countered_action.damage_dealt == 0 else str(_calculate_true_damage(attacker)) + " Damage Dealt")
  })
  else:
    effect_label.text = "Missed Counter!"
  
