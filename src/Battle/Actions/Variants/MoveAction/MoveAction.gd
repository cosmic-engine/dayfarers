class_name MoveAction
extends Action

### Action variant where a move is used.

## The move used on this action.
var move # Move

## The targets of the move used.
var targets # Combatant[]

## The AP cost of the move used.
var ap_cost # Int

## Reverts the state of the subject to that when this action was performed.
func restore():
  subject.selected_move = move
  subject.selected_targets = targets
