class_name RestAction
extends Action

### Action where the user restores AP by siphoning it
# from the time tree.

## The amount of AP to add to the user.
var ap_restored # Int

## Determines the new stats of one combatant after this action is
# performed.
func create_stats(combatant): # Combatant -> Stats
  # if combatant is the subject and they increase their ap
  var final_ap = combatant.stats.ap + ap_restored
  if (final_ap > combatant.stats.max_ap):
    final_ap = combatant.stats.max_ap
  
  if combatant == subject:
    return Factories.Stats.create_stats(
      combatant.stats.max_hp,
      combatant.stats.hp,
      combatant.stats.max_ap,
      final_ap,
      combatant.stats.defense,
      combatant.stats.speed,
      combatant.stats.stat_modifiers.next(),
      combatant.stats.status_effect.next(), 
      combatant
    )
  # if combatant is a bystander
  else:
    return _create_default_stats(combatant)

## Reverts the state of the subject to that when this action was performed.
func restore():
  assert(false)

## Gets a user-facing message describing the action in detail.
func get_message(): # -> String
  return ("{combatant} has rested and regained {ap_restored}AP".format({
    "combatant": subject.display_name,
    "ap_restored": ap_restored
  }))

## Whether this action is equivalent to other.
func equals(other): # Action -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.subject == subject and
    other.ap_restored == ap_restored
  )

## Updates the UI with action information.
func _draw_info():
  var user_portrait = $"Node2D/ActionContainer/ActionBGTexture/CasterIcon"
  var _targets = $"Node2D/ActionContainer/ActionBGTexture/TargetsVBox"
  var _move_name_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AbilityLabel"
  var _move_icon = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/AttackIcon"
  var effect_label = $"Node2D/ActionContainer/ActionBGTexture/AbilityVBox/EffectLabel"

  user_portrait.texture = subject.portrait
  effect_label.text = "Restored {ap_restored} AP".format({
    "ap_restored": ap_restored
  })
