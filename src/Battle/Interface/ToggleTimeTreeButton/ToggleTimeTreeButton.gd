extends Button
### The button responsible for toggling the visibility and functionality
# of the Timme Tree.

## A reference to the game node.
onready var Game = get_tree().get_root().get_node("Game") # Node
## The node responsible for playing sound effects.
onready var SFX = get_node("Sound Effect") # Node

## Makes this button invisible when it is loaded into the game.
func _enter_tree():
  visible = false

## Toggles the time tree's visibility when this button is clicked.
func _on_pressed():
  SFX.play()
  #Game.battle.time_tree.visible = !Game.battle.time_tree.visible
  Game.battle.time_tree.toggle_visibility()
