class_name BattleLog
extends Control

### Maintains a history of actions in text format of the battle.

## Messages in the log.
var messages = []

## Appends a message to the log.
func add_message(message):
  messages.append(message)
