extends Control

### Tutorial UI element that appears at the start of battles.

## Reference to the SFT player node.
onready var SFX = get_node("Sound Effect") # Node

## Play the SFX and hide visiblity when start is pressed.
func _on_StartButton_pressed():
  SFX.play()
  visible = false

func _on_UINextButton_pressed():
  pass # Replace with function body.

## Show the instructions when the instructions button is clicked.
func _on_InstructionToggleButton_button_up():
  SFX.play()
  visible = true
