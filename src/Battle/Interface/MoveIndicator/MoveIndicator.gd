extends Spatial

### UI element that floats above the combatant's head.
# Represents the action that the combatant is about
# to take. Can be obscured.

## Reference to the texture of the undetermined move.
export(Texture) var undetermined_texture # Texture

## Reference to the tecture of the determined move.
export(Texture) var determined_texture # Texture

## Reference to the target combatant portrait node.
onready var target_portrait = $"Viewport/GUI/TargetPicture" # Node

## Reference to the label showing the move display name.
onready var move_label = $"Viewport/GUI/MoveLabel" # Node

## Reference to the mesh instance of the indicator.
onready var mesh_instance = $"MeshInstance" # Node

## Whether to mirror the indicator on the Z axis.
var is_flipped = false # Boolean

## Validate object on ready.
func _ready():
  assert(target_portrait != null)
  assert(move_label != null)

## Reveal the target portrait.
func show_target(thinking_combatant): # Combatant
  target_portrait.texture = thinking_combatant.selected_targets[0].portrait

## Reveal the move display name.
func show_move(thinking_combatant): # Combatant
  move_label.set_text(thinking_combatant.selected_move.display_name)

## Reveal both the target portrait and the move name in the indicator.
func show_all(thinking_combatant): # Combatant
  show_target(thinking_combatant)
  show_move(thinking_combatant)

## Show the ! texture in place of the portrait.
func show_determined():
  target_portrait.texture = determined_texture

## Show the ? texture in place of the portrait.
func show_undetermined():
  target_portrait.texture = undetermined_texture

## Reset the move label.
func clear_move():
  move_label.set_text("")
  
## Reset the move label and the portrait.
func clear_all():
  show_undetermined()
  clear_move()

## Make visible.
func show():
  visible = true

## Make invisible.
func hide():
  visible = false
