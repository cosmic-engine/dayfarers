extends Control
### Represents a state of the battle at a given point in time.
# Each turn is displayed on the time tree as a button with
# all combatant's stats labeled on them.

## A signal emitted when a turn is selected, emitting the selected turn.
signal on_select(turn_panel)

## A label used to store the font of a turn button
onready var _temp_label = $TurnButton/Label # Node

# onready var hp_label = $TurnButton/TurnInfoHBoxContainer/PlayerInfoVBox/CharacterInfo1/APHPVBox/HPProgressBar/HPLabel # Node
## The node that displays the turn's current player.
onready var player_info = $TurnButton/TurnInfo/PlayerInfo # Node
## The node that displays the turn's current player's target.
onready var enemy_info = $TurnButton/TurnInfo/EnemyInfo # Node
## The node responsible for playing sound effects.
onready var SFX = get_node("Sound Effect") # Node

## A reference to the action that led to a turn.
var _previous_action # Action
## A list of all actions that have been taken so far on a turn.
var _next_actions = [] # Action[]
## A map of each combatant and their stats on a turn.
var stats # { Combatant: Stats }
## The font used to display the turn's information.
var font # DynamicFont
## Stores whether or not a turn is the first in a round.
var _is_round_start = false # Boolean

## The UI button representing a turn on the time tree.
var _turn_button # Node

## Disables the turn button if it didn't start a round and displays
# the stats associated with the turn.
func _ready():
  if !_is_round_start:
    $TurnButton.disabled = true
  _turn_button = $TurnButton
  font = _temp_label.get_font("")
  update_display()

## Changes the stats displayed on the turn button to the updated
# stats values.
func update_display():
  var i = 0
  for combatant in stats.keys():
    var stat = stats[combatant]
    if i < 3:
      player_info.get_child(i).set_stats(combatant, stat)
    else:
      enemy_info.set_stats(combatant, stat)
    i = i + 1

## Assigns a turn's previous action.
func set_previous_action(action): # Action
  _previous_action = action

## Gets a turn's previous action.
func get_previous_action(): # -> Action
  return _previous_action

## Assigns a button node to a turn.
func set_button(button): # Node
  _turn_button = button

## Gets a turn's button node.
func get_button(): # -> Node
  return _turn_button

## Adds an action to a turn's next action array, and sets that
# action's previous and next turn values.
func add_next_action(next_action, next_turn): # Action, Turn
  _next_actions.append(next_action)
  next_action.previous_turn = self
  next_action.next_turn = next_turn
  next_turn.set_previous_action(next_action)

## Checks if a specific action exists in a turn's next actions array
# and returns it if found.
func has_next_action(action): # Action -> Action
  for next_action in _next_actions:
    if action.equals(next_action):
      return next_action
  return null

## Returns a turns next action array.
func get_next_actions(): # -> Action[]
  return _next_actions

## Checks if a turn has any next actions.
func has_next(): # -> Boolean
  return len(_next_actions) != 0

# Sets turn button to green
func set_active_style():
  $TurnButton.get_node("CurrentTurnOverlay").visible = true

# Sets turn button to default color
func set_inactive_style():
  $TurnButton.get_node("CurrentTurnOverlay").visible = false

## Enables the turn button if the turn was the first in a round.
func mark_as_round_start():
  _is_round_start = true
  $TurnButton.disabled = false
# $TurnButton.visible = true

## Checks if a turn is the first in a round.
func is_round_start(): # -> Boolean
  return _is_round_start

## Emits the on_select signal when a a turn button is clicked.
func _on_TurnButton_Clicked():
  SFX.play()
  emit_signal("on_select", self)
