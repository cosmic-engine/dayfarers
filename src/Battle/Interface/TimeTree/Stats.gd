class_name Stats
extends Node

### Combatant unique values for combat.

## The max HP of the combatant.
var max_hp # Int

## The current hp of the combatant.
var hp # Int

## The max AP of the combatant.
var max_ap # Int

## The current AP of the combatant.
var ap # Int

## The current defense of the combatant.
var defense # Int

## The current speed of the combatant.
var speed # Int

## The current stat modifiers of the combatant.
var stat_modifiers # StatModifiers

## The current status effect of the combatant.
var status_effect # StatusEffect

## Whether the combatant is dead.
func is_dead(): # -> Boolean
  return hp <= 0
