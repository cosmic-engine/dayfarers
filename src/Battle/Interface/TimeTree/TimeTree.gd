extends Control
### The Time Tree stores all turns and actions that occur in a Battle.
# It displays all of turns and actions with the appropriate information,
# and allows specific turns to be chosen for time travel or syphoning.

## A reference the game node.
onready var Game = get_tree().get_root().get_node("Game") # Node
## A reference to the factories node.
onready var Factories = get_tree().get_root().get_node("Factories") # Node
var Row = preload("res://Battle/Interface/TimeTree/Row.tscn") # Row

## A signal emitted when time travel occurs.
signal time_traveled()
## A signal emitted when a turn is selected, emitted with the selectged turn.
signal _selected_turn(turn)

## Determines if the player is currently time travelling.
var is_time_traveling = true # Boolean

## The active turn in the time tree.
var _active_turn # Turn
## The width of the time tree in terms of grid units.
var _tree_width = 1 # Int
## A reference to the top level node that stores the time tree's turns.
var _table_root # Node
## The first turn in the time tree.
var _start_turn # Turn

## A node that displays instructions to the player to select a turn from the tree.
onready var message = $"Message"
## The button that closes the time tree when clicked.
onready var cancel_button = $"CancelButton"

## Sets the table root and message text, then makes the time tree invisible by default.
func _ready():
  message.text = "Select a turn to time travel to!"
  _table_root = $ScrollContainer/VBoxContainer
  visible = false

  assert(_table_root != null, "Could not find table root.")

  var row = _add_row()
  row.add_empty()
  var turn = _create_first_turn()
  row.insert_turn(turn, 0)
  turn.set_active_style()
  _active_turn = turn
  _start_turn = turn

#region === State Management Logic ===

## Advances the current turn to the appropriate next one, or creates a new one
# if the action taken doesn't already exist in the current turn's next action array.
func handle_action(action): # Action
  assert(_active_turn != null, "_active_turn should not be null.")

  var next_action = _active_turn.has_next_action(action)
  if next_action != null:
    _set_active_turn(next_action.next_turn)
    _update_stats(next_action.next_turn.stats)
  else:
    _active_turn.add_child(action)
    var stats = action.create_all_stats()
    var turn = Factories.Turn.create_turn(stats, self)
    _add_turn(action, turn)
    _draw_action_info(action)
    _update_stats(stats)

## Lets the player to choose a turn from the time tree for certain moves.
func prompt_for_turn():
  message.text = "Select a turn to siphon from!"
  is_time_traveling = false
  visible = true
  return yield(self, "_selected_turn")

## Cancels prompting the player to select a turn to choose for certain move requirements.
func stop_prompt_for_turn():
  visible = false
  is_time_traveling = true
  message.text = "Select a turn to time travel to!"

## Changes all combatants' stats to match the input ones.
func _update_stats(stats): # Stats
  for member in Game.battle.get_combatants():
    member.stats = stats[member]

## Returns the time tree's active turn.
func get_active_turn(): # -> Turn
  return _active_turn

## Toggles the visibility of the time tree.
func toggle_visibility():
  visible = !visible
  call_deferred("update_scroll")
  
## Scrolls the time tree to show the active turn on screen.
func update_scroll():
  get_node("ScrollContainer").scroll_horizontal = (_active_turn.margin_right  - _active_turn.rect_size.x - (rect_size.x / 2))
  get_node("ScrollContainer").scroll_vertical = _active_turn.get_node("..").rect_position.y - _active_turn.get_node("..").rect_size.y

## Iterates through all turns and draws the lines connecting them.
func _draw_action_info(action): # Action
  var current = _start_turn
  var todo = [current]
  while !todo.empty():
    current = todo.pop_front()
    for current_action in current.get_next_actions():
      var next_turn = current_action.next_turn
      todo.append(next_turn)
      
      var from_to_dictionary = _calculate_time_tree_node_vectors(current,next_turn)
      
      var from = from_to_dictionary["from"]
      var to = from_to_dictionary["to"]

      #---------------------- TEMP ----------------------#
      # Final version should instead set combatant,move, #
      # and target portraits                             #
      
      var center
      
      if len(action.previous_turn.get_next_actions()) == 1:
        center = Vector2((from.x + to.x)/2 , (from.y + to.y)/2)
        _position_action_details_container(current_action, action, center, to)
      else:
        center = Vector2((from.x + to.x)/2 - 50, (from.y + to.y)/2)
        _position_action_details_container(current_action, action, center, to)
      
      #---------------------- TEMP ----------------------#
      
      _draw_action_line(current_action, from, to, center)
        
## Manages the math of finding the from and to points of 
# the current and next_turn nodes
# returns the from and to locations as a Dictionary
func _calculate_time_tree_node_vectors(current,next_turn): # Turn, Turn -> Dict<Vector2>
  var current_parent_row_index = current.get_parent().get_index()
  var current_turn_index = current.get_index()
  var current_turn_rect_size_x = 500 #current.get_size().x
  var current_turn_rect_size_y = 200 #current.get_size().y
  #var current_turn_button = current.get_button()
  var current_turn_button_size_x = 250 #current_turn_button.get_size().x
  var current_turn_button_size_y = 200 #current_turn_button.get_size().y
  
  var next_parent_row_index = next_turn.get_parent().get_index()
  var next_turn_index = next_turn.get_index()
  var next_turn_rect_size_x = 500 #next_turn.get_size().x
  var next_turn_rect_size_y = 200 #next_turn.get_size().y
  #var next_turn_button = next_turn.get_button()
  #var next_turn_button_size_x = 200 #next_turn_button.get_size().x
  var next_turn_button_size_y = 200 #next_turn_button.get_size().y
  
  var from = Vector2(
    (current_turn_index * current_turn_rect_size_x + current_turn_button_size_x) - (current_turn_index * next_turn_rect_size_x),
    (current_parent_row_index * current_turn_rect_size_y + current_turn_button_size_y / 2) - (current_parent_row_index * next_turn_rect_size_y)
  )
  
  var to = Vector2(
    (next_turn_index * next_turn_rect_size_x) - (current_turn_index * next_turn_rect_size_x),
    (next_parent_row_index * next_turn_rect_size_y + next_turn_button_size_y / 2)
    - (current_parent_row_index * next_turn_rect_size_y)
  )
  
  var from_to = { 
    "from" : from,
    "to" : to
  }
  
  return from_to

## Takes in positional data about the location of a container,
# and decides where to place current_action container
func _position_action_details_container(current_action, action, center, to): # Action, Action, Vector2, Vector2
  var container = current_action.get_node("Node2D/ActionContainer")
  if len(action.previous_turn.get_next_actions()) == 1:
    container.rect_position = Vector2(center.x - container.get_size().x/2 , to.y  - container.get_size().y/2)
  else:
    container.rect_position = Vector2(center.x + 100 - container.get_size().x/2 , to.y  - container.get_size().y/2)

## Takes in positional data about the location of nodes,
# and decides how to draw the action line
func _draw_action_line(current_action, from, to, center):
  var line = current_action.get_node("ActionLine")
  line.clear_points()

  var points = PoolVector2Array()

  points.append(from)
  points.append(Vector2(center.x,from.y))
  points.append(Vector2(center.x, to.y))
  points.append(to)

  for point in points:
    line.add_point(point)
  

# ------------ TEMP : Testing if the modifications made when bring up the time travel menu breaks the logic ------------ #
## Resets each turn's margins, position and size.
func _clear_time_tree_node_modifications():
  var current = _start_turn
  var todo = [current]
  while !todo.empty():
    current = todo.pop_front()

    current.margin_top = 0.0
    current.margin_left = 0.0
    current.margin_right = 0.0
    current.margin_bottom = 0.0
    current.set_position(Vector2(0.0,0.0))
    current.set_size(Vector2(500.0,150.0))
    
    for curr_action in current.get_next_actions():
      var next_turn = curr_action.next_turn
      todo.append(next_turn)

## Creates the time tree's first turn.
func _create_first_turn(): # -> Turn
  var stats = {}

  for combatant in Game.battle.get_combatants():
    var combatant_stats = create_default_stats(combatant)
    combatant.stats = combatant_stats
    stats[combatant] = combatant_stats

  var turn = Factories.Turn.create_turn(stats, self)
  turn.mark_as_round_start()
  return turn

## Creates a standard stats object.
func create_default_stats(combatant): # Combatant -> Stats
  var healthy = Factories.StatusEffect.create_healthy(combatant)
  var stat_modifiers = Factories.Stats.create_stat_modifiers(0, 0, 0, 0, 0, 0)
  return Factories.Stats.create_stats(
    combatant.default_max_hp,
    combatant.default_max_hp,
    combatant.default_max_ap,
    combatant.default_max_ap,
    combatant.default_defense,
    combatant.default_speed,
    stat_modifiers,
    healthy, 
    combatant
  )

## Sets the time tree's active turn.
func _set_active_turn(turn): # Turn
  assert(turn != null, "turn should not be null.")
  assert(_active_turn != null, "_active_turn should not be null.")

  # Revert the button style to normal.
  _active_turn.set_inactive_style()
  # Reassign the active turn to the new turn.
  _active_turn = turn
  # Set the button style to be green.
  _active_turn.set_active_style()

## Adds the given turn to the current turn via the action provided.
# Will create a new timeline if a next action exists on the current turn.
# Will add the turn to the current timeline if there is no existing next action
# on the current turn.
func _add_turn(action, turn): # Action, Turn
  assert(action != null, "Action cannot be null.")
  assert(turn != null, "Turn cannot be null.")
  assert(_active_turn != null, "Active turn cannot be null.")

  if _active_turn.has_next():
    _add_branching_turn(turn)
  else:
    _add_sequential_turn(turn)

  # Now that the turn has been placed on the table, move to that turn.
  _active_turn.add_next_action(action, turn)

  _set_active_turn(turn)

## The function called when the turn selected signal is received.
func _on_turn_selected(turn):
  if is_time_traveling:
    _time_travel(turn)
  else:
    emit_signal("_selected_turn", turn)

## Changes the active turn to the one given, and updates all stats accordingly.
func _time_travel(turn): # Turn
  assert(turn != null, "turn should not be null.")

  _set_active_turn(turn)
  _clear_time_tree_node_modifications()
  _update_stats(turn.stats)
  emit_signal("time_traveled")

#endregion

#region === Turn Node Positioning Logic ===

## Creates a new row, adds it to the table, and returns it.
func _add_row(): # -> Row
  var row = Row.instance()
  _table_root.add_child(row)
  return row

## Adds a new turn on the same timeline as the current turn.
# Will make the tree wider if necessary.
func _add_sequential_turn(turn): # Turn
  # If the tree isn't wide enough to fit the next turn, make it wider.
  # Keep the number of columns in each row equal to the width of the table.
  if _tree_width - 1 <= _active_turn.get_index():
    _tree_width += 1
    _align_rows()

  # Add the new turn to the right of the active turn on the tree,
  # and make the new turn the active turn.
  _active_turn.get_parent().insert_turn(turn, _active_turn.get_index() + 1)

## Adds a new turn on a different branch from the current turn.
# Will make the tree taller by two rows.
func _add_branching_turn(turn): # Turn
  # Find the upper bound of the branches from the current turn,
  # and add an empty row above it.
  var highest_row = _get_highest_row_in_turn_sequence(_active_turn)
  var empty_row_1 = _add_row()
  _move_row_above(highest_row, empty_row_1)

  # Find the lower bound of the branches from the current turn,
  # and add an empty row below it.
  var lowest_row = _get_lowest_row_in_turn_sequence(_active_turn)
  var empty_row_2 = _add_row()
  _move_row_below(lowest_row, empty_row_2)

  # Make the empty rows have the same column count as the tree width.
  for _i in range(_tree_width):
    empty_row_1.add_empty()
    empty_row_2.add_empty()

  # Move every turn in every branch from the current turn up by one row.
  _move_turns_up_one_row(_active_turn)

  # Add the new branch two rows below the bottom branch row.
  empty_row_2.insert_turn(turn, _active_turn.get_index() + 1)

## Makes the given empty row the immediate older sibling to the given row.
func _move_row_above(row, empty_row): # Row, Row
  _table_root.move_child(empty_row, row.get_index())

## Makes the given empty row the immediate younger sibling to the given row.
func _move_row_below(row, empty_row): # Row, Row
  _table_root.move_child(empty_row, row.get_index() + 1)

## Returns a list of rows in the table.
func _get_rows(): # Row[]
  return _table_root.get_children()

# Adds a column to the table by adding a single empty to every row.
func _align_rows():
  for row in _get_rows():
    row.add_empty()

## Takes all turns from every branch of the given turn (excluding the given turn)
# and moves them up by exactly one row.
func _move_turns_up_one_row(turn): # Turn
  var row = turn.get_parent()
  var above_row = row.get_parent().get_child(row.get_index() - 1)

  # Move every turn except the active turn.
  if turn != _active_turn:
    row.swap_by_index(turn.get_index(), above_row)

  # Recursively call for every branch from the given turn.
  for action in turn.get_next_actions():
    _move_turns_up_one_row(action.next_turn)

## Returns the row at the lowest position to the right of the given turn.
func _get_lowest_row_in_turn_sequence(turn): # Turn -> Row
  if !turn.has_next():
    return turn.get_parent()
  return _get_lowest_row_in_turn_sequence(turn.get_next_actions()[-1].next_turn)

## Returns the row at the highest position to the right of the given turn.
func _get_highest_row_in_turn_sequence(turn): # Turn -> Row
  if !turn.has_next():
    return turn.get_parent()
  return _get_highest_row_in_turn_sequence(turn.get_next_actions()[0].next_turn)

#endregion

## Cancels target selection when clicked.
func _on_CancelButton_button_up():
  $"CancelButton/Sound Effect".play()
  if is_time_traveling:
    visible = false
  else:
    stop_prompt_for_turn()
    Game.battle.cancel_player_targets_select()
