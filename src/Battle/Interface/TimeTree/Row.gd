extends HBoxContainer

### A row of turns and empty spaces in the time tree.

var Turn = preload("res://Battle/Interface/TimeTree/Turn.tscn") # Node
var Empty = preload("res://Battle/Interface/TimeTree/Empty.tscn") # Node

## Inserts turn at index, swaps the empty node at target location with current turn node.
func insert_turn(turn, index): # Turn, Int
  var empty = get_child(index)
  remove_child(empty)
  empty.queue_free()
  add_child(turn)
  move_child(turn, index)

## Switch two rows position at a given index.
func swap_by_index(index, other_row): # Int, Row
  var turn = get_child(index)
  remove_child(turn)
  other_row.insert_turn(turn, index)
  var empty = add_empty()
  move_child(empty, index)

## Create an empty row.
func add_empty(): # -> Empty
  var empty = Empty.instance()
  add_child(empty)
  return empty
