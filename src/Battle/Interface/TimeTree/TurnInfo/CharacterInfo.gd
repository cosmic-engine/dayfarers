extends HBoxContainer

### UI component aggregated by the time tree turn nodes.
# Displays combatant stats and status effects.

## Combatant portrait
onready var portrait = $PortraitTextureRect # Node

## Combatant status label
onready var status_icon = $StatusVBox/StatusTextureRect # Node

## Combatant HP label
onready var hp_label = $APHPVBox/HPProgressBar/HPLabel # Node

## Combatant AP label
onready var ap_label = $APHPVBox/APProgressBar/APLabel # Node

## Combatant HP progress bar
onready var hp_progress_bar = $APHPVBox/HPProgressBar # Node

## Combatant AP progress bar
onready var ap_progress_bar = $APHPVBox/APProgressBar # Node

## Update the stats of the given combatant in the UI
func set_stats(combatant, stats): # Combatant, Stats
  portrait.texture = combatant.portrait
  status_icon.texture = combatant.stats.status_effect.status_icon
  hp_label.bbcode_text = "[center]{hp} / {max_hp}[/center]".format({
    "hp": stats.hp,
    "max_hp": stats.max_hp
  })
  ap_label.bbcode_text = "[center]{ap} / {max_ap}[/center]".format({
    "ap": stats.ap,
    "max_ap": stats.max_ap
  })
  hp_progress_bar.max_value = stats.max_hp
  hp_progress_bar.value = stats.hp
  ap_progress_bar.max_value = stats.max_ap
  ap_progress_bar.value = stats.ap
