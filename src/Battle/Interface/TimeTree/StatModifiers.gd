class_name StatModifiers
extends Node

### Buffs and debuffs that are applied to combatant stats.

## Reference to the factories singleton.
onready var Factories = get_tree().get_root().get_node("Factories") # Node

## The amount by which to modify damage.
var damage_modifier # Int

## The duration of the damage modifier.
var damage_modifier_duration # Int

## The amount by which to modify defense.
var defense_modifier # Int

## The duration of the defense modifier.
var defense_modifier_duration # Int

## The amount by which to modify speed.
var speed_modifier # Int

## The duration of the speed modifier.
var speed_modifier_duration # Int

## Returns a reference to a new stat modifier, equal to the current one.
func current(): # -> StatModifiers
  return Factories.Stats.create_stat_modifiers(
    damage_modifier,
    damage_modifier_duration,
    defense_modifier,
    defense_modifier_duration,
    speed_modifier,
    speed_modifier_duration
  )

## Returns a reference to a new stat modifier, one ahead of the current one.
func next(): #  -> StatModifiers
  var new_damage_modifier
  var new_damage_modifier_duration = damage_modifier_duration - 1

  if new_damage_modifier_duration == 0:
    new_damage_modifier = 0
  else:
    new_damage_modifier = damage_modifier
  
  var new_defense_modifier
  var new_defense_modifier_duration = defense_modifier_duration - 1

  if new_defense_modifier_duration == 0:
    new_defense_modifier = 0
  else:
    new_defense_modifier = defense_modifier
  
  var new_speed_modifier
  var new_speed_modifier_duration = speed_modifier_duration - 1

  if new_speed_modifier_duration == 0:
    new_speed_modifier = 0
  else:
    new_speed_modifier = speed_modifier

  return Factories.Stats.create_stat_modifiers(
    new_damage_modifier,
    new_damage_modifier_duration,
    new_defense_modifier,
    new_defense_modifier_duration,
    new_speed_modifier,
    new_speed_modifier_duration
  )
