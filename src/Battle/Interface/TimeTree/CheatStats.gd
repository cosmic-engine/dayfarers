extends Stats

### Variant of the stats class specifically for the cheater boss.
# The combatant will only die if their HP reaches exactly zero.

## Whether the combatant can perform another action.
func is_dead():
  return hp == 0
