extends Spatial

### UI element that indicates which combatant is being targetted.

## Reference to the active combatant turn indicator.
var _active_turn_arrow # Node

## Reference to the available target indicator.
var _available_target_arrow # Node

## Reference to the hover combatant target indicator.
var _hover_target_arrow # Node

## Assigns arrows on ready.
func _ready():
  _active_turn_arrow = $TurnArrow
  _available_target_arrow = $TargetAvailableArrow
  _hover_target_arrow = $TargetSelectedArrow
  #rotation.y = get_viewport().get_camera().rotation.y
  hide_arrows()

## Hide all arrows except the active turn arrow.
func show_active_turn_arrow():
  _active_turn_arrow.visible = true
  _available_target_arrow.visible = false
  _hover_target_arrow.visible = false

## Hide all arrows except the available target arrow.
func show_available_target_arrow():
  _active_turn_arrow.visible = false
  _available_target_arrow.visible = true
  _hover_target_arrow.visible = false

## Hide all arrows except the hover target arrow.
func show_hover_target_arrow():
  _active_turn_arrow.visible = false
  _available_target_arrow.visible = false
  _hover_target_arrow.visible = true

## Hide all arrows.
func hide_arrows():
  _active_turn_arrow.visible = false
  _available_target_arrow.visible = false
  _hover_target_arrow.visible = false
