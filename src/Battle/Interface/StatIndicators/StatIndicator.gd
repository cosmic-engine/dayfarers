extends VBoxContainer

### UI element that displays all information about a single combatant.

## Reference to the label with the combatant display name.
onready var name_label = $CombatantName # Node

## Reference to the image with the combatant portrait.
onready var portrait = $ActiveCombatantContainer/CharacterPortrait # Node

## Reference to the image with the combatant portrait.
onready var current_turn = $ActiveCombatantContainer/StatBGTexture/CurrentTurnBGTexture

## Reference to the label with the combatant HP.
onready var hp_label = $ActiveCombatantContainer/StatContainer/HealthLabelContainer/HPLabel # Node

## Reference to the progress bar with the combatant HP.
onready var hp_bar = $ActiveCombatantContainer/CircleHealthBar # Node

## Reference to the label with the combatant AP.
onready var ap_label = $ActiveCombatantContainer/StatContainer/APLabelContainer/APLabel # Node

## Reference to the progress bar with the combatant AP.
onready var ap_bar = $ActiveCombatantContainer/CircleHealthBar/CircleAPBar # Node

## Reference to the label with the combatant status effect display name.
onready var status_label = $StatusBoxBGTexture/StatusVBox/StatusEffect # Node

## Reference to the label with the combatant status effect display name.
onready var status_icon = $StatusBoxBGTexture/StatusIcon # Node

## Reference to the label with the combatant stat modifiers.
onready var stat_modifier_label = $StatusBoxBGTexture/StatusVBox/StatModifier # Node

## Reference to the combatant.
onready var combatant = $".." # Combatant

## Call update stats once the combatant stats change on ready.
func _ready():
  combatant.connect("stats_changed", self, "_update_stats")
  # _update_stats()

## Move the node to the given parent.
func reparent(new_parent): # Node 
  var parent = get_parent()
 # current_turn.set_visible(false)
  parent.remove_child(self)
  new_parent.add_child(self)

## Update all stats displayed on the indicator.
func _update_stats():
 # _update_current_turn()
  _update_name()
  _update_portrait()
  _update_max_hp()
  _update_hp()
  _update_max_ap()
  _update_ap()
  _update_status()
  _update_stat_modifier()

## Update the display name on the UI.
func _update_name():
  name_label.text = combatant.display_name
  
func   _update_current_turn():
  current_turn.set_visible(true)
  pass

## Update the portrait on the UI.
func _update_portrait():
  portrait.texture = combatant.portrait

## Update the max HP on the UI.
func _update_max_hp():
  if (combatant.stats.hp < 0): # special liar only conditional
    hp_bar.max_value = 0
    hp_bar.min_value = -combatant.stats.max_hp
    hp_bar.fill_mode = 4
  else:
    hp_bar.min_value = 0
    hp_bar.max_value = combatant.stats.max_hp
    hp_bar.fill_mode = 5

## Update the HP on the UI.
func _update_hp():
  var hp = combatant.stats.hp
  hp_label.text = str(hp)
  hp_bar.value = hp
  if (hp < 0): # special liar only conditional
    hp_bar.set_tint_progress(Color(1, 1, 1, 1))
    hp_bar.set_tint_under(Color(.21, .53, .08, 1))
  else:
    hp_bar.set_tint_progress(Color(.67,.01,.01, 1))
    hp_bar.set_tint_under(Color(1, 1, 1, 1))

## Update the max AP on the UI.
func _update_max_ap():
  ap_bar.max_value = combatant.stats.max_ap

## Update the AP on the UI.
func _update_ap():
  var ap = combatant.stats.ap
  ap_label.text = str(ap)
  ap_bar.value = ap

## Update the status effect name on the UI.
func _update_status():
  status_icon.texture = combatant.stats.status_effect.status_icon
  status_label.text = combatant.stats.status_effect.display_name

## Update the stat modifiers on the UI.
func _update_stat_modifier():
  stat_modifier_label.text = "A: {atk}, D: {def}, S: {spd}".format({
    "atk": combatant.stats.stat_modifiers.damage_modifier,
    "def": combatant.stats.stat_modifiers.defense_modifier,
    "spd": combatant.stats.stat_modifiers.speed_modifier
  })
