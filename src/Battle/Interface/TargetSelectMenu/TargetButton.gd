extends Button

### Button that represents a combatant to be targetted.

## Signal that emits once a combatant has been selected.
signal on_select(combatant) # Combatant

## The combatant this button represents.
var _combatant # Combatant

## Constructor.
func init(combatant): # Combatant
  _combatant = combatant
  text = combatant.name

## Emits the on select signal when button is clicked.
func _on_pressed():
  emit_signal("on_select", _combatant)
