extends Control

### UI element that allows the player to select a target from the combatants in battle.

var TargetButton = preload("res://Battle/Interface/TargetSelectMenu/TargetButton.tscn")

## Reference to the game singleton.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

# Emits once a target has been chosen by the player.
signal targets_selected(combatants) # Combatant[]

## The combatant selecting targets.
var _current_combatant # Combatant

## The combatants being targetted.
var _target_combatants # Combatant[]

## Whether all target combatants are being targetted at once.
var _target_all # Boolean

## Do not process inputs on ready.
func _ready():
  set_process_input(false)
  visible = false

## Handle player input.
func _input(event): # Event
  if _target_all:
    _process_input_for_selecting_all_targets(event)
  else:
    _process_input_for_selecting_a_single_target(event)

## Display the target select menu with the given target combatants.
func open(target_combatants, target_all): # Combatant[], Boolean
  _target_combatants = target_combatants
  _target_all = target_all
  visible = true

  if _target_all:
    _target_all_combatants()
  else:
    _target_single_combatant()

  set_process_input(true)

## Close the target select menu.
func close():
  visible = false
  for combatant in _target_combatants:
    combatant.target_indicator.hide_arrows()
    combatant.disconnect("hover_start", self, "_on_combatant_hover_start")
  set_process_input(false)

## Mark every combatant as a target.
func _target_all_combatants():
  for combatant in _target_combatants:
    combatant.target_indicator.show_hover_target_arrow()

## Mark a single combatant as a target.
func _target_single_combatant():
  for combatant in _target_combatants:
    combatant.target_indicator.show_available_target_arrow()
    if OK != combatant.connect("hover_start", self, "_on_combatant_hover_start"):
      assert(false, "Failed to connect signal.")
  _current_combatant = _target_combatants[0]
  _current_combatant.target_indicator.show_hover_target_arrow()

## Move the cursor to the combatant provided.
func _move_to_combatant(combatant): # Combatant
  _current_combatant.target_indicator.show_available_target_arrow()
  _current_combatant = combatant
  _current_combatant.target_indicator.show_hover_target_arrow()

## Emit the signal after selecting the targets.
func _target_selected(combatants): # Combatant[]
  SFX.play()
  emit_signal("targets_selected", combatants)

## Handle inputs when move is a single target move.
func _process_input_for_selecting_a_single_target(event): # Event
  if event.is_action_released("ui_up"):
    var above_combatant = _current_combatant.spawn_point.above_spawn_point.combatant
    if above_combatant in _target_combatants:
      _move_to_combatant(above_combatant)
  elif event.is_action_released("ui_down"):
    var below_combatant = _current_combatant.spawn_point.below_spawn_point.combatant
    if below_combatant in _target_combatants:
      _move_to_combatant(below_combatant)
  elif event.is_action_released("ui_left"):
    var left_combatant = _current_combatant.spawn_point.left_spawn_point.combatant
    if left_combatant in _target_combatants:
      _move_to_combatant(left_combatant)
  elif event.is_action_released("ui_right"):
    var right_combatant = _current_combatant.spawn_point.right_spawn_point.combatant
    if right_combatant in _target_combatants:
      _move_to_combatant(right_combatant)
  elif (
    event.is_action_released("target_confirm") ||
    (
      event.is_action_released("target_confirm_mouse") &&
      _current_combatant.hovering
    )
  ):
    _target_selected([_current_combatant])

## Handle input for moves that target all combatants.
func _process_input_for_selecting_all_targets(event): # Event
  if event.is_action_released("target_confirm"):
    _target_selected(_target_combatants)

## Move cursor to combatant when hovering on them.
func _on_combatant_hover_start(combatant): # Combatant 
  _move_to_combatant(combatant)

## Cancel target select when clicking button.
func _on_CancelButton_button_up():
  SFX.play()
  Game.battle.cancel_player_targets_select()
