extends Button


### An Effectful Resource
# Plays a sound and an animation
# Only used by the Tutorial Screen
# FUTURE: Rename to `TutorialScreenButton`


## Where we play animations from
onready var animation_player = $"../AnimationPlayer" # AnimationPlayer

## Where we play the sound effect from
onready var SFX = get_node("Sound Effect") # Node

## Simple check for which page in the tutorial we are on
var is_toggled = true # Boolean


## When the button is pressed
# Play the animation and effects
# (Button signal connected in scene file)
func _on_button_up():
  pass;
  

func _on_BossPageButton_button_up():
  SFX.play()
  if is_toggled:
    animation_player.play("FlipPageBoss")


func _on_TTPageButton_button_up():
  SFX.play()
  if is_toggled:
    animation_player.play("FlipPageTT")


func _on_AbilitiesPageButton_button_up():
  SFX.play()
  if is_toggled:
    animation_player.play("FlipPageAbilities") 
