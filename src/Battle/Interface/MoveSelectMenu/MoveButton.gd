extends Button

### Button that stores a move for a combatant to use,
# selected by the player.

## Signal that emits once a move has been selected.
signal selected(move) # Move

#warning-ignore:unused_signal
## Signal that emits once the button is being hovered by the mouse.
signal start_hover(move) # Move

#warning-ignore:unused_signal
## Signal that emits once the button stops being hovered by the mouse.
signal stop_hover()

## The move that the button represents.
var move # Move

## Reference to the label with the move display name.
onready var move_name_label = $"AbilityNameText" # Node

## Reference to the label with the move AP cost.
onready var move_cost_label = $"AbilityCostText" # Node

## Reference to the image with the move icon.
onready var move_icon_image = $"AbilityIcon" # Node

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Sey button state and message on ready.
func _ready():
  assert(move_name_label != null, "Could not find move_name_label")
  assert(move_cost_label != null, "Could not find move_cost_label")
  assert(move_icon_image != null, "Could not find move_icon_image")

  _set_button_state()
  _set_message()

# Sets the button label to display name, ap cost, and desc.
func _set_message():
  move_name_label.bbcode_text = "[center]{text}[/center]".format({
      "text": move.display_name
  })
  move_cost_label.bbcode_text = "[center]{text}[/center]".format({
      "text": str(move.ap_cost)
  })
  move_icon_image.texture = move.icon

## EMit selected signal when clicked.
func _on_pressed():
  SFX.play()
  yield(get_tree().create_timer(0.4), "timeout")
  emit_signal("selected", move)

## Enable or disable the button depending on AP value.
func _set_button_state():
  if move.user.stats.ap < move.ap_cost:
    disabled = true
  else:
    disabled = false

## Emit start hover signal when mouse is over button.
func _on_mouse_entered():
  grab_focus()
  emit_signal("start_hover", move)

## Emit stop hover signal when mouse is not over button.
func _on_mouse_exited():
  emit_signal("stop_hover")
