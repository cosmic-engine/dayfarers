extends Control

### Menu for a single combatant where the player can choose a move
# for that combatant to use.

var MoveButton = preload("res://Battle/Interface/MoveSelectMenu/MoveButton.tscn")

# Emits once after a single move has been chosen by the player.
signal move_selected(move) # Move

## Reference to the combatant that is selecting a move.
var _combatant # Combatant

# Creates a list of moves known by the provided combatant to the player.
func open(combatant): # Combatant
  _combatant = combatant
  
  var first = true
  for move in _combatant.moves:
    var move_button = MoveButton.instance()
    move_button.move = move
    move_button.connect("selected", self, "_move_selected")
    move_button.connect("start_hover", self, "_show_description")
    # move_button.connect("stop_hover", self, "_hide_description")
    $MoveSelectMenuBackground/MoveListScrollContainer/MoveList.add_child(move_button)
    if first:
      first = false
      move_button.grab_focus()

  visible = true

# Removes the existing menu buttons and disconnects all signals.
func close():
  visible = false
  _combatant = null
  for child in $MoveSelectMenuBackground/MoveListScrollContainer/MoveList.get_children():
    # remove_child(child)
    child.queue_free()
    
# When any of the move buttons are pressed, emit the move_selected signal
# and remove the menu.
func _move_selected(move): # Move
  emit_signal("move_selected", move)

## Display the description of the move.
func _show_description(move): # Move
  $MoveSelectMenuBackground/MoveDescriptionRect/DescriptionLabel.text = move.description

## Hide the description of the move.
func _hide_description():
  $MoveSelectMenuBackground/MoveDescriptionRect/DescriptionLabel.text = ""
