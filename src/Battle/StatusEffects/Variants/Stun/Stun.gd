class_name Stun
extends StatusEffect

### Status effect where the combatant has a chance to be stunned.

## The probabilty that confuse will be applied each turn.
export var chance : int # Int

## How many turns to apply the status effect for.
var duration # Int

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  .apply()
  yield(get_tree(), "idle_frame")

  var action

  if _is_applied():
    action = Factories.Action.create_stun_action(combatant)
  else:
    action = yield(combatant.use_move(), "completed")

  Game.battle.turn_indicator.text = action.get_message()
  Game.battle.time_tree.handle_action(action)

## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  return Factories.StatusEffect.create_stun([combatant], duration)[0]

## Returns a reference to a new status effect, one ahead of the current one.
func next(): # -> StatusEffect
  if duration == 1:
    return Factories.StatusEffect.create_healthy(combatant)
  return Factories.StatusEffect.create_stun([combatant], duration - 1)[0]

## Whether the status effect is to be applied this turn, according to the chance.
func _is_applied(): # Boolean
  #return (randi() % 100) < chance
  return true

## Whether this status effect is equivalent to another.
func equals(other): # StatusEffect -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.combatant == combatant and
    other.chance == chance and
    other.duration == duration
  )
