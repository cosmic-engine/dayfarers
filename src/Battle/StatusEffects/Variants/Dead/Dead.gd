class_name Dead
extends StatusEffect

### Status effect variant where the subject cannot perform any additional actions.

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  .apply()
  yield(get_tree(), "idle_frame")
  var action = Factories.Action.create_dead_action(combatant)
  Game.battle.time_tree.handle_action(action)

## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  return Factories.StatusEffect.create_dead(combatant)

## Returns a reference to a new status effect, one ahead of the current one.
func next(): # -> StatusEffect
  return current()

## Whether this status effect is equivalent to another.
func equals(other): # StatusEffect -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.combatant == combatant
  )
