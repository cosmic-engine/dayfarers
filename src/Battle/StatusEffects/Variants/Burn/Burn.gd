class_name Burn
extends StatusEffect

### Deals damage every turn to the inflicted combatant.

## Damage applied.
var damage # Int

## How many turns to apply the status effect for.
var duration # Int

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  .apply()
  var action = yield(combatant.use_move(), "completed")
  deal_damage()
  Game.battle.turn_indicator.text = action.get_message()
  Game.battle.time_tree.handle_action(action)

## Subtract damage from HP of user.
func deal_damage():
  combatant.stats.hp -= damage

## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  return Factories.StatusEffect.create_burn([combatant], duration, damage)[0]

## Returns a reference to a new status effect, one ahead of the current one.
func next(): # -> StatusEffect
  if duration == 1:
    return Factories.StatusEffect.create_healthy(combatant)
  return Factories.StatusEffect.create_burn([combatant], duration - 1, damage)[0]

## Whether this status effect is equivalent to another.
func equals(other): # StatusEffect -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.combatant == combatant and
    other.damage == damage and
    other.duration == duration
  )
