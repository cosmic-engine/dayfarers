class_name Confused
extends StatusEffect

### Status effect variant where the subject has a chance to hit themselves
# with their move rather than their intended target.

## The probabilty that confuse will be applied each turn.
export var chance : int # Int

## How many turns to apply the status effect for.
var duration # Int

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  .apply()
  update_move()
  var action = yield(combatant.use_move(), "completed")
  Game.battle.turn_indicator.text = action.get_message()
  Game.battle.time_tree.handle_action(action)

## Whether the status effect is to be applied this turn, according to the chance.
func _is_applied(): # Boolean
  return true#(randi() % 100) < chance

## Redirect the move to the user rather than the target, only if confuse is applied.
func update_move():
  if (_is_applied()):
    if (combatant.selected_move.move_type == GlobalEnum.move_type.Attack || 
    combatant.selected_move.move_type == GlobalEnum.move_type.StatusEffectAttack || 
    combatant.selected_move.move_type == GlobalEnum.move_type.Modifier):
      if (combatant.selected_move.target_all):
        combatant.selected_targets = combatant.party.combatants
      else:
        combatant.selected_targets = [combatant.party.combatants[randi() % combatant.party.combatants.size()]]
    elif (combatant.selected_move.move_type == GlobalEnum.move_type.Restorative):
      if (combatant.selected_move.target_all):
        combatant.selected_targets = Game.battle.enemy_party.combatants
      else:
        print("here")
        combatant.selected_targets = [Game.battle.enemy_party.combatants[randi() % Game.battle.enemy_party.combatants.size()]]
        print(combatant.selected_targets)
  
## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  return Factories.StatusEffect.create_confused([combatant], duration)[0]

## Returns a reference to a new status effect, one ahead of the current one.
func next(): # -> StatusEffect
  if duration == 1:
    return Factories.StatusEffect.create_healthy(combatant)
  return Factories.StatusEffect.create_confused([combatant], duration - 1)[0]

## Whether this status effect is equivalent to another.
func equals(other): # StatusEffect -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.combatant == combatant and
    other.chance == chance and
    other.duration == duration
  )
