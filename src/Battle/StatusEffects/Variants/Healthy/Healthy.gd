class_name Healthy
extends StatusEffect

### Status effect variant where the user uses moves without impediments.

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  .apply()
  var action = yield(combatant.use_move(), "completed")
  Game.battle.turn_indicator.text = action.get_message()
  Game.battle.time_tree.handle_action(action)

## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  return Factories.StatusEffect.create_healthy(combatant)

## Returns a reference to a new status effect, one ahead of the current one.
func next(): # -> StatusEffect
  return current()

## Whether this status effect is equivalent to another.
func equals(other): # StatusEffect -> Boolean
  return (
    other.get_class_type() == get_class_type() and
    other.combatant == combatant
  )
