class_name StatusEffect
extends Node

### Command pattern for combatant when taking their turn.

onready var Game = get_tree().get_root().get_node("Game") # Node
onready var Factories = get_tree().get_root().get_node("Factories") # Node

export var display_name : String # String
## User facing icon representing the status effect.
export var status_icon: Texture # Texture
export var status_effect_effect : String # String

var combatant # Combatant

## Combatant takes their turn, applying the effect of this status effect.
func apply():
  combatant.model.play_effect(status_effect_effect)

## Returns a reference to a new status effect, equal to the current one.
func current(): # -> StatusEffect
  assert(false)

## Returns a reference to a new status effect, one ahead of the current one.
func next():  # -> StatusEffect
  assert(false)

## Whether this status effect is equivalent to another.
func equals(_other): # StatusEffect -> Boolean
  assert(false)

## Returns the class name of this status effect.
func get_class_type(): # -> String
  return get_script().get_path().get_file().split(".", true, 1)[0]
