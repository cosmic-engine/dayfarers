extends Spatial

### The core gameplay logic of Dayfarers. Facilitates turn based combat,
# move and target selection logic, and win or lose conditions.

## A signal that when used to tell subscribers when
# a round has started.
signal start_round()

## A signal that when used to tell subscribers when
# a game is won.
signal battle_won()

## A signal that when used to tell subscribers when
# a game is lost.
signal battle_lost()

## A reference to the music singleton.
onready var Music = get_tree().get_root().get_node("Music") # Node

## A reference to the time tree, which holds all the
# states of the current battle for the player to jump
# to.
onready var time_tree = $"BattleInterface/TimeTree" # Node

## A reference to the menu that displays the abili-
# ties a combatant can use.
onready var move_select_menu = $"BattleInterface/MoveSelectMenu" # Node

## A reference to the menu that displays the other
# combatants the current combatant can select.
onready var target_select_menu = $"BattleInterface/TargetSelectMenu" # Node

## A reference to the control that handles showing
# the details of the actions during move execution.
onready var turn_indicator = $"BattleInterface/BattleLogContainer/TurnIndicator" # Node

## A reference to the button on screen that toggles
# the time travel menu in the time tree.
onready var toggle_time_tree_button = $"BattleInterface/ToggleTimeTreeButton" # Node

## The party of combatants controlled by the player.
var player_party # Party

## The party of combatants not controlled by the player.
var enemy_party # Party

## The scene where the fight takes place.
var arena # Arena

## A reference to the combatant who can currently
# perform an action.
var thinking_combatant # Combatant

## A reference to the combatants who can are still
# alive in the battle.
var thinking_combatants # Combatant[]

## The action that an enemy will perform next.
var future_action # Action

## Godot’s lifecycle hook. Called when the instantiated
# Node is added to the Node Tree. Is used to set attributes
# at runtime as well as trigger the first turn.
func _ready():
  assert(time_tree != null)
  assert(move_select_menu != null)
  assert(target_select_menu != null)
  assert(turn_indicator != null)
  assert(toggle_time_tree_button != null)

  if OK != connect("start_round", self, "_start_round"):
    assert(false)
  if OK != time_tree.connect("time_traveled", self, "_on_time_travel"):
    assert(false)

  _position_stat_indicators()
  Music.play(arena.get_song())
  emit_signal("start_round")

## Creates and places stat indicator UI elements.
func _position_stat_indicators():
  var player_stat_indicators = $"BattleInterface/PlayerStatIndicators"
  var enemy_stat_indicators = $"BattleInterface/EnemyStatIndicators"

  for combatant in player_party.combatants:
    combatant.stat_indicator.reparent(player_stat_indicators)

  for combatant in enemy_party.combatants:
    combatant.stat_indicator.reparent(enemy_stat_indicators)

## Begins processing for a new round.
func _start_round():
  _show_move_indicators()
  _determine_enemy_actions()

## Returns a list of combatants that are able to perform actions.
func _determine_thinking_combatants(party): # Party -> Combatant[]
  var party_dup = party.combatants.duplicate(true)
  var thinking = []
  for combatant in party_dup:
    if !combatant.stats.is_dead():
      thinking.append(combatant)
  return thinking

## Allows enemy combatants to make a move choice for their turn.
func _determine_enemy_actions():
  thinking_combatants = _determine_thinking_combatants(enemy_party)
  _determine_enemy_action()

## Allows player combatants to make a move choice for each of their combatants.
func _determine_player_actions():
  toggle_time_tree_button.visible = true
  thinking_combatants = _determine_thinking_combatants(player_party)
  _determine_player_action()

## Allows an enemy combatant to make a move choice for their turn.
# Will make the same move choice if a future already exists.
func _determine_enemy_action():
  thinking_combatant = thinking_combatants.pop_front()

  if thinking_combatant == null:
    _determine_player_actions()
    return

  thinking_combatant.move_indicator.clear_all()
  thinking_combatant.target_indicator.show_active_turn_arrow()

  if time_tree.get_active_turn().has_next():
    _find_future_action(time_tree.get_active_turn())
    future_action.restore()
    thinking_combatant.move_indicator.show_all(thinking_combatant)
    thinking_combatant.target_indicator.hide_arrows()
    _determine_enemy_action()
    return

  _start_enemy_move_select()

## If a future exists for the given turn, find the action that was taken.
func _find_future_action(turn): # Turn
  future_action = null
  var action = turn.get_next_actions()[0]
  if action.subject == thinking_combatant:
    future_action = action
    return
  _find_future_action(action.next_turn)

## Allows a player combatant to make a move choice for their turn.
func _determine_player_action():
  thinking_combatant = thinking_combatants.pop_front()

  if thinking_combatant == null:
    _execute_actions()
    return
  
  thinking_combatant.target_indicator.show_active_turn_arrow()
  thinking_combatant.stat_indicator.current_turn.visible = true

  _start_player_move_select()

## Allows enemy combatants to pick targets for their move.
func _start_enemy_move_select():
  if OK != thinking_combatant.connect("move_selected", self, "_on_enemy_move_selected"):
    assert(false, "Could not connect signal.")
  thinking_combatant.select_move()

## When an enemy combatant picks their move, start their target select phase.
func _on_enemy_move_selected():
  _end_enemy_move_select()
  _start_enemy_targets_select()

## Stops the enemy combatant from picking more moves.
func _end_enemy_move_select():
  if thinking_combatant.is_connected("move_selected", self, "_on_enemy_move_selected"):
    thinking_combatant.disconnect("move_selected", self, "_on_enemy_move_selected")
  else:
    assert(false, "move_selected signal was never connected.")

## Start the enemy combatant target select phase.
func _start_enemy_targets_select():
  thinking_combatant.target_indicator.show_active_turn_arrow()
  if OK != thinking_combatant.connect("targets_selected", self, "_on_enemy_targets_selected"):
    assert(false, "Could not connect signal.")
  thinking_combatant.select_targets()

## When an enemy combatant picks their targets, start the next action determine phase.
func _on_enemy_targets_selected():
  _end_enemy_targets_select()
  thinking_combatant.move_indicator.show_determined()
  thinking_combatant.target_indicator.hide_arrows()
  _determine_enemy_action()

## Stops the enemy from picking any more targets.
func _end_enemy_targets_select():
  if thinking_combatant.is_connected("targets_selected", self, "_on_enemy_targets_selected"):
    thinking_combatant.disconnect("targets_selected", self, "_on_enemy_targets_selected")
  else:
    assert(false, "targets_selected signal was never connected.")

## Allows player combatants to pick targets for their move.
func _start_player_move_select():
  thinking_combatant.target_indicator.show_active_turn_arrow()
  if OK != thinking_combatant.connect("move_selected", self, "_on_player_move_selected"):
    assert(false, "Could not connect signal.")
  thinking_combatant.select_move()

## When a player combatant picks their move, start their target select phase.
func _on_player_move_selected():
  _end_player_move_select()
  _start_player_targets_select()

## Stops the player combatant from picking more moves.
func _end_player_move_select():
  if thinking_combatant.is_connected("move_selected", self, "_on_player_move_selected"):
    thinking_combatant.disconnect("move_selected", self, "_on_player_move_selected")
  else:
    assert(false, "move_selected signal was never connected.")

## Start the player combatant target select phase.
func _start_player_targets_select():
  toggle_time_tree_button.visible = false
  if OK != thinking_combatant.connect("targets_selected", self, "_on_player_targets_selected"):
    assert(false, "Could not connect signal.")
  thinking_combatant.select_targets()

## When a player combatant picks their targets, start the next action determine phase.
func _on_player_targets_selected():
  _end_player_targets_select()
  thinking_combatant.move_indicator.show_all(thinking_combatant)
  thinking_combatant.target_indicator.hide_arrows()
  thinking_combatant.stat_indicator.current_turn.visible = false
  _determine_player_action()

## Stops the player from picking any more targets.
func _end_player_targets_select():
  thinking_combatant.close_target_select_menu()
  toggle_time_tree_button.visible = true
  if thinking_combatant.is_connected("targets_selected", self, "_on_player_targets_selected"):
    thinking_combatant.disconnect("targets_selected", self, "_on_player_targets_selected")
  else:
    assert(false, "targets_selected signal was never connected.")

## Interrupts player target select phase.
func cancel_player_targets_select():
  _end_player_targets_select()
  _start_player_move_select()

## Once all moves and targets are selected, execute all of the selections
# for this round. Pass records into the time tree, and start a new round.
func _execute_actions():
  toggle_time_tree_button.visible = false

  var sorted = _get_combatants_sorted_by_speed_descending()
  sorted = _get_combatants_sorted_by_move_priority(sorted)
  for combatant in sorted:
    combatant.move_indicator.clear_all()
    combatant.move_indicator.hide()
    yield(combatant.take_turn(), "completed")
    if _is_battle_won():
      emit_signal("battle_won")
      return
    elif _is_battle_lost():
      emit_signal("battle_lost")
      return

  time_tree.get_active_turn().mark_as_round_start()
  for combatant in get_combatants():
    if combatant.play_dead:
      combatant.play_dead = false
      combatant.model.play_animation_backwards("ApplyDeath")
      yield(combatant.model.get_animation_player(), "animation_finished")
      combatant.model.play_animation("ApplyIdle")
  
  emit_signal("start_round")

## Process player inputs.
func _input(_event): # InputEvent
  if Input.is_action_just_released("ui_end"):
    emit_signal("battle_won")

## Callback to handle time travel request from time tree.
func _on_time_travel():
  _end_player_move_select()
  _reset_move_indicators()
  thinking_combatant.cancel_select_move()
  time_tree.visible = false
  _reset_combatants()
  emit_signal("start_round")

## Draw move selected indicators above combatants.
func _show_move_indicators():
  for combatant in get_combatants():
    combatant.move_indicator.show()

func _reset_move_indicators():
  for combatant in get_combatants():
    combatant.move_indicator.clear_all()
    combatant.move_indicator.hide()

## Revert player combatants when time traveling.
func _reset_combatants():
  for combatant in player_party.combatants:
    thinking_combatant.target_indicator.hide_arrows()
    thinking_combatant.stat_indicator.current_turn.visible = false

## Returns a list of combatants sorted by speed, descending.
func _get_combatants_sorted_by_speed_descending(): # Combatant[]
  var sorted = get_combatants().duplicate(true)
  sorted.sort_custom(self, "_compare_combatant_speeds")
  return sorted

## Determines which combatant is faster.
func _compare_combatant_speeds(first_combatant, second_combatant): # Combatant, Combatant -> Boolean
  if first_combatant.stats.speed != second_combatant.stats.speed:
    return first_combatant.stats.speed > second_combatant.stats.speed
  return randi() == 0

## Returns a list of combatants sorted by move priority, descending.
func _get_combatants_sorted_by_move_priority( combatants ): # Combatant[] -> Combatant[]
  var sorted = combatants.duplicate(true)
  sorted.sort_custom(self, "_compare_move_priority")
  return sorted

## Determines which combatant's selected move has a higher priority.
func _compare_move_priority(first_combatant, second_combatant): # Combatant, Combatant -> Boolean
  if first_combatant.selected_move.move_priority > second_combatant.selected_move.move_priority:
    return true
  return false

## Whether the battle ended in player victory.
func _is_battle_won(): # -> Boolean
  for member in enemy_party.combatants:
    if not member.stats.is_dead():
      return false
  return true

## Whether the battle ended in player defeat.
func _is_battle_lost(): # -> Boolean
  for member in player_party.combatants:
    if not member.stats.is_dead():
      return false
  return true

## Returns a reference to the opposing party of a given party.
func get_opposing_party(combatant): # Combatant -> Party
  if combatant in player_party.combatants:
    return enemy_party
  return player_party

## The list of all combatants in the battle.
func get_combatants(): # -> Combatant[]
  return player_party.combatants + enemy_party.combatants
