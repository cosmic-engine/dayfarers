class_name Party
extends Node

### Group of combatants with a name.

## The user facing display name of the party.
export var display_name: String # String

## The list of combatants within the party.
var combatants = [] # Combatant[]

## Parses the combatants into the party on ready.
func _ready():
  for combatant in get_node("Combatants").get_children():
    combatants.append(combatant)
