class_name CharacterModel
extends Spatial

### Manipulate and interact with character models, including
# playing animations or particle effects.

## The player for the character animations.
var _animation_player # AnimationPlayer

## The player for the character particle effects.
var _effect_player # AnimationPlayer

## Get references to the players on ready.
func _ready():
  _animation_player = get_child(0).get_node("AnimationPlayer")
  _effect_player = get_node("EffectPlayer")

  assert(
    _animation_player != null,
    "_animation_player could not be found."
  )

## Whether an animation name exists on the animation player.
func has_animation(animation_name): # String -> Boolean
  return _animation_player.has_animation(animation_name)

## Plays a given animation name.
func play_animation(animation_name): # String
  if(_animation_player.has_animation(animation_name)):
    _animation_player.play(animation_name)

## Plays a given animation name backwards.
func play_animation_backwards(animation_name): # String
  if(_animation_player.has_animation(animation_name)):
    _animation_player.play_backwards(animation_name)

## Reference to the animation player.
func get_animation_player(): # -> AnimationPlayer
  return _animation_player

## Plays a particle effect with the given effect name.
func play_effect(effect_name): # String
  if (_effect_player.has_animation(effect_name)):
    _effect_player.play(effect_name)

## Reference to the particle effect player
func get_effect_player(): # -> AnimationPlayer
  return _effect_player
