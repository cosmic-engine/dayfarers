extends Node




### Inter-Scene Music Player
# Handles music between scenes
# Must be loaded as a SINGLETON
# Problem: Checking if a scene has changed  
# (Such as from main menu to level select)  
# Solution: Add Signals to Main Menu Buttons
# TODO: Copy AudioStreamPlayer API and Paste here




## Alert anyone interested what piece of music to anticipate next
# Also allows others to request new music by simply emitting a signal
# stream: AudioStream
signal triggernextpart(stream)




# Main Menu Audio

## Convenient Access to Main Menu Intro Music
export var main_intro:       AudioStreamSample # AudioStreamSample

## Convenient Access to Main Menu Loop Music
export var main_stream:      AudioStreamSample # AudioStreamSample




# Victory Screen Audio

## Convenient Access to Victory Screen Intro Music
export var victory_intro:    AudioStreamSample # AudioStreamSample

## Convenient Access to Victory Screen Loop Music
export var victory_stream:   AudioStreamSample # AudioStreamSample




# Defeat Screen Audio

## Convenient Access to Defeat Screen Loop Music
export var defeat_stream:    AudioStreamSample # AudioStreamSample




# Level Market/Thief Audio

## Convenient Access to Market/Thief Intro Music
export var perpetual_intro:  AudioStreamSample # AudioStreamSample

## Convenient Access to Market/Thief Loop Music
export var perpetual_stream: AudioStreamSample # AudioStreamSample




# Level Amphitheater/Liar Audio

## Convenient Access to Amphitheater/Liar Intro Music
export var cued_intro:       AudioStreamSample # AudioStreamSample

## Convenient Access to Amphitheater/Liar Loop Music
export var cued_stream:      AudioStreamSample # AudioStreamSample




# Level Alley/Cheat Audio

## Convenient Access to Alley/Cheat Intro Music
export var rifts_intro:      AudioStreamSample # AudioStreamSample

## Convenient Access to Alley/Cheat Loop Music
export var rifts_stream:     AudioStreamSample # AudioStreamSample




# Level UNKNOWN Audio

## Convenient Access to UNKNOWN Intro Music
# Possibly DEPRECATED/UNUSED
export var boss_menu_intro:  AudioStreamSample # AudioStreamSample

## Convenient Access to UNKNOWN Loop Music
# Possibly DEPRECATED/UNUSED
export var boss_menu_stream: AudioStreamSample # AudioStreamSample

## Convenient Access to UNKNOWN Outro Music
# Possibly DEPRECATED/UNUSED
export var boss_menu_outro:  AudioStreamSample # AudioStreamSample





# Music Player Controls

## The current active audio player
var music: AudioStreamPlayer # AudioStreamPlayer

## Which bus for pan/volume/and other inherited aspects
var _audio_bus_name = "Music" # String

## UNUSED/DEPRECATED let others quickly check if any music is actively playing
# Directly check the `music` variable instead
var is_playing = true # Boolean

## The current selected track of music, may or may not be playing
var current_stream: AudioStreamSample # AudioStreamSample


## Load all of the resources for the Singleton
# Create and Configure music
# Connects Signals and Instantiates AudioPlayback Children
func _ready():
  # Load Child Nodes
  music = AudioStreamPlayer.new()
  music.set_bus(_audio_bus_name)
  add_child(music)
  # Connect Signals
  if OK != connect("triggernextpart", self, "_on_trigger_next_part"):
    assert(false, "Could not connect signal.")
  if OK != music.connect("finished", self, "_on_AudioStreamPlayer_finished"):
    assert(false, "Could not connect signal.")




## FIXME FUNCDOC
func _on_trigger_next_part(stream): # AudioStream
  music.stream = stream
  stream.loop_mode = AudioStreamSample.LOOP_FORWARD
  stream.loop_begin = 0
  stream.loop_end = stream.get_length() * stream.mix_rate
  music.play()




## Pauses/Unpauses the music playback
func pause(pause): # Bool
  music.set_stream_paused(pause)




## Globally play a piece of music
# Note, can handle arguments of different type
# Both Strings and Samples can be loaded
func play(next_theme): # AudioStreamSample
  
  # Reset variables and update tracking variables
  var next_stream
  current_stream = next_theme
  
  # Check the argument type and load appropriately
  if next_theme is AudioStreamOGGVorbis:
    next_stream = next_theme
  elif next_theme is AudioStreamSample:
    next_stream = next_theme
  if next_theme is String:
    next_stream = load(next_theme)
  
  # for special segments of audio, do not play them normally
  if next_stream == main_intro:
    if music.stream != main_stream:
      music.stream = next_stream
      music.play()
  elif next_stream == rifts_intro:
    music.stream = next_stream
    music.play()
  elif next_stream == victory_intro:
    music.stream = next_stream
    music.play()
  elif next_stream == boss_menu_intro:
    music.stream = next_stream
    music.play()
  elif next_stream == boss_menu_outro:
    music.stream = next_stream
    music.play()
  elif next_stream == boss_menu_stream:
    music.stream = next_stream
    music.play()
  elif next_stream == perpetual_intro:
    music.stream = next_stream
    music.play()
  elif next_stream == cued_intro:
    music.stream = next_stream
    music.play()
  
  # For other music tracks, handle normally
  else:  
    emit_signal("triggernextpart", next_stream)
    



## Handle Special Segments of Music
# Such as Intro, Loop, End, Etc
# Does nothing on regular tracks of music
# Uses pointer comparison
func _on_AudioStreamPlayer_finished():
  if current_stream == main_intro:
    emit_signal("triggernextpart", main_stream)
  elif current_stream == rifts_intro:
    emit_signal("triggernextpart", rifts_stream)
  elif current_stream == victory_intro:
    emit_signal("triggernextpart", victory_stream)
  elif current_stream == victory_stream:
    emit_signal("triggernextpart", victory_stream)
  elif current_stream == boss_menu_intro:
    play(boss_menu_stream)
  elif current_stream == boss_menu_outro:
    play(boss_menu_intro)
  elif current_stream == boss_menu_stream:
    play(boss_menu_outro)
  elif current_stream == cued_intro:
    emit_signal("triggernextpart", cued_stream)
  elif current_stream == cued_stream:
    emit_signal("triggernextpart", cued_stream)
  elif current_stream == perpetual_intro:
    emit_signal("triggernextpart", perpetual_stream)
  elif current_stream == perpetual_stream:
    emit_signal("triggernextpart", perpetual_stream)

