extends Spatial


### Allow easy sound effect requests from other nodes
# Wraps an AudioStreamPlayer and allows method-calling access
# (As opposed to not having access otherwise)
# USed in many UI scenes


## The sound effect we play, saved to the scene and chosen from editor
# NOTE! Prefer `wav` format files where possible, `ogg` WILL loop
export (AudioStreamSample) var path  # AudioStreamSample

## Where we play sound effects from
var player: AudioStreamPlayer # AudioStreamPlayer


## Instantiate a new player and configure it
func _ready():
  player = AudioStreamPlayer.new()
  add_child(player)
  player.set_bus("Effects")
  player.stream = path

## Play the sound effect
# Similar to a "OneShot" method
func play():
  if player.stream != null:
    player.play()
  else:
    print("Nothing Here, add a path to " + self.get_parent().name)
