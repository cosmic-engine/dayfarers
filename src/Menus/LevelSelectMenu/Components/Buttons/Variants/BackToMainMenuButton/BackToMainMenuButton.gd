class_name BackToMainMenuButton
extends LevelSelectMenuButton

### Closes the level select menu and returns to the main menu.

onready var Root = get_tree().get_root() # Node
onready var Game = Root.get_node("Game") # Node
onready var SFX = get_node("Sound Effect") # Node

## Unloads the level select menu and loads the main menu on click.
func _on_button_up():
  SFX.play()
  yield(get_tree().create_timer(0.4), "timeout")
  var level_select_menu = Root.get_node("LevelSelectMenu")
  Root.remove_child(level_select_menu)
  level_select_menu.queue_free()
  var title_screen = load("res://Menus/MainMenu/MainMenu.tscn").instance()
  Root.add_child(title_screen)
