class_name StartBattleButton
extends LevelSelectMenuButton

### Creates an encounter with the specified boss.

## Reference to the game singleton.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Reference to the encounter scene.
export var encounter_scene: PackedScene # PackedScene

## Creates an encounter on button up with the encounter scene.
func _on_button_up():
  SFX.play()
  Game.create_encounter(encounter_scene)
