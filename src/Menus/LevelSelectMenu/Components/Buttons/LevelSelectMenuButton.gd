class_name LevelSelectMenuButton
extends Button

### Button on the level select menu.

## Reference to the music singleton.
onready var Music = get_tree().get_root().get_node("Music") # Node

## Play the animatic when the replay video button is clicked.
func _on_ReplayVideoButton_button_up():
  var animatic = load("res://Menus/Animatic/Animatic.tscn").instance()
  Music.pause(true)
  get_tree().get_root().add_child(animatic)
  get_parent().queue_free()
