extends AnimatedSprite

var speed = 4

func _process(_delta):
  position.x += speed
  if position.x >= 1344:
    queue_free()
