extends Control

### Menu that allows the player to select which fight to start.

## Reference to the game singleton.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Reference to the cheater button node.
onready var cheater_button = $TheCheaterStartBattleButton # Node

## Reference to the liar button node.
onready var liar_button = $TheLiarStartBattleButton # Node

## Refernce to the thief button node.
onready var thief_button = $TheThiefStartBattleButton # Node

## Reference to the victory screen node.
onready var victory_screen = $GameVictoryScreen # Node

## Stores whether or not the easter egg has been triggered.
onready var _egg_triggered = false

## Load the end screen if all victory flags are set on ready.
func _ready():
  cheater_button.disabled = Game.victories["cheater"]
  liar_button.disabled = Game.victories["liar"]
  thief_button.disabled = Game.victories["thief"]
  
  if (
    Game.victories["cheater"] and
    Game.victories["liar"] and
    Game.victories["thief"]
  ):
    _load_end_screen()

## Display the victory screen.
func _load_end_screen():
  victory_screen.visible = true

## Handles input given to the scene.
func _input(event):
  if !_egg_triggered:
    if event is InputEventKey and event.pressed and $EggTimer.time_left == 0:
      # if key is shift
      if event.scancode == 16777237:
        $EggTimer.start()

    if event is InputEventKey and !event.pressed:
      $EggTimer.stop()
  
  else:
    $EggTimer.stop()

## Triggers the menu's easter egg.
func _easter_egg():
  _egg_triggered = true
  var green_guy = load("res://Menus/LevelSelectMenu/Components/Egg/GreenGuy.tscn").instance()
  green_guy.position = Vector2(-64, 656)
  add_child(green_guy)
