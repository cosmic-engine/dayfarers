extends Button

### Terminates the game from the main menu.

## Quits the game on click.
func _on_button_up():
  get_tree().quit()
