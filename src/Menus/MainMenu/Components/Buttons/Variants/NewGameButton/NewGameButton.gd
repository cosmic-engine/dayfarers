extends Button

### Button that loads the level select screen.

var LevelSelect = preload("res://Menus/LevelSelectMenu/LevelSelectMenu.tscn") # PackedScene

## Reference to SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Instantiate the level seelct menu scene.
func _create_level_select_menu():
  var level_select_menu = LevelSelect.instance()
  $"/root".add_child(level_select_menu)

## Load level select menu and unload main menu on button click.
func _on_button_up():
  SFX.play()
  yield(get_tree().create_timer(0.4), "timeout")
  _create_level_select_menu()
  #Music.play(Music.boss_menu_intro)
  get_node("/root/MainMenu").queue_free()
