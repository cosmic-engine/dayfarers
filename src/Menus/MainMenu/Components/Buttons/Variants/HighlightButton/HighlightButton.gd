extends Node

### Title screen button that changes styles when hovered over.

## Path to the default font.
export(String, FILE) var defaultFont # (String,File)

## Reference to the default font.
onready var _defaultFont: DynamicFont = load(defaultFont) # DynamicFont

## Path to the highlighted font.
export(String, FILE) var highlightedFont # (String,File)

## Reference to the highlighted font.
onready var _highlightedFont: DynamicFont = load(highlightedFont) # DynamicFont

## Reference to the root of the scene.
onready var _parent: Button = get_node("..") # Node

## Set the font to the hover font when the mouse starts hovering.
func _on_Button_mouse_entered():
  _parent.add_font_override("font",_highlightedFont)

## Set the font to the default font when the mouse stops hovering.
func _on_Button_mouse_exited():
  _parent.add_font_override("font",_defaultFont)
