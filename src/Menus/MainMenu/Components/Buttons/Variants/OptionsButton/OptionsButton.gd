extends Button

### Button the loads the options menu.

var OptionsMenu = preload("res://Menus/OptionsMenu/OptionMenu.tscn") # PackedScene

## Reference to the options menu node.
export (NodePath) var optionsMenu # NodePath

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Show the options menu.
func _create_options_menu():
  var _op = get_node(optionsMenu)
  _op.set_visible(true)

## Show the options menu on button click.
func _on_button_up():
  SFX.play()
  _create_options_menu()
