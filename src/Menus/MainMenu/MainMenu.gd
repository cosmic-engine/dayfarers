extends Control

### The title screen. Displays the current version number and name.

## Reference to the music singleton.
onready var Music = get_tree().get_root().get_node("Music") # Node

## Reference to the game version name label.
onready var _version_name_label = $HBoxContainer/VersionName # Node

## Reference to the game version number label.
onready var _version_number_label = $HBoxContainer/VersionNumber # Node

## Get the version number and play the menu theme on ready.
func _ready():
  _get_version_number()
  Music.play( Music.main_intro )

## Get the version number of the game.
func _get_version_number():
  var file = File.new()
  file.open("res://../RELEASE", File.READ)
  var version_name = file.get_line()
  var version_number = file.get_line()
  file.close()

  _version_name_label.text = version_name
  _version_number_label.text = version_number
