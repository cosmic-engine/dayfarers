extends Control

### UI screen that appears when the player completes all battles.

## Reference to tree root.
onready var Root = get_tree().get_root() # Node

## Reference to game singleton.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Clears game progress flags and returns to title scren.
func _on_ResetGameButton_button_up():
  Game.victories["liar"] = false
  Game.victories["cheater"] = false
  Game.victories["thief"] = false
  var level_select_menu = Root.get_node("LevelSelectMenu")
  Root.remove_child(level_select_menu)
  level_select_menu.queue_free()
  var title_screen = load("res://Menus/MainMenu/MainMenu.tscn").instance()
  Root.add_child(title_screen)

## Quit game on button click.
func _on_ExitGameButton_button_up():
  get_tree().quit()
