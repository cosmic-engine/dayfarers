extends Control

### Shown to the user when pressing ESC.
# Stops the game processing while active.

## Reference to the options menu node.
onready var options_menu = $"OptionsMenu"

## Reference to the game singleton node.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Sets the pause mode process on ready.
func _ready():
  visible = false
  pause_mode = Node.PAUSE_MODE_PROCESS

## Toggle pause when the pause input is made.
func _input(_event): # Event
  if Input.is_action_just_released("ui_pause"):
    _toggle_pause()

## Toggle between pause and unpause state.
func _toggle_pause():
  if visible:
    unpause()
  else:
    pause()

## Pause the game processing.
func pause():
  Game.battle.get_node("BattleInterface").visible = false
  visible = true
  get_tree().paused = true
  
## Resume the game processing.
func unpause():
  Game.battle.get_node("BattleInterface").visible = true
  visible = false
  options_menu.visible = false
  Game.battle.get_node("BattleInterface").visible = true
  get_tree().paused = false
