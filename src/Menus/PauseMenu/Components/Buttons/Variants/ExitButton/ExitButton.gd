extends Button

### Terminates the game from the pause menu.

## Quits the game on click.
func _on_button_up():
  get_tree().quit()
