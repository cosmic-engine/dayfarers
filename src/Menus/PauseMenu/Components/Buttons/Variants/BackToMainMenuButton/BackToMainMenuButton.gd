extends Button

### Closes the pause menu and returns to the main menu.

onready var Game = get_tree().get_root().get_node("Game") # Scene
onready var Music = get_tree().get_root().get_node("Music") # Scene
onready var SFX = get_node("Sound Effect") # Node

export var pause_menu_path : NodePath # Node

## Unloads the pause menu and loads the main menu on click.
func _on_button_up():
  SFX.play()
  var level_select_menu = load("res://Menus/LevelSelectMenu/LevelSelectMenu.tscn").instance()
  get_node(pause_menu_path).unpause()
  get_tree().get_root().add_child(level_select_menu)
  Game.end_battle()
  Music.play(Music.boss_menu_intro)
