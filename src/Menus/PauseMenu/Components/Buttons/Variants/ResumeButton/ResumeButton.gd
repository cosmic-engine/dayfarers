extends Button

### Unpauses the game.

## Reference to the pause menu node.
onready var pause_menu = $"../../../.." # Node

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Unpause the game on click.
func _on_button_up():
  SFX.play()
  pause_menu.unpause()
