extends Button

### Closes the game over menu and returns to the main menu.

onready var Root = get_tree().get_root() # Node
onready var Game = Root.get_node("Game") # Node
onready var Music = get_tree().get_root().get_node("Music") # Scene
onready var SFX = get_node("Sound Effect") # Node

## Unloads the game over menu and loads the main menu on click.
func _on_button_up():
  SFX.play()
  Game.end_battle()
  var level_select_menu = load("res://Menus/LevelSelectMenu/LevelSelectMenu.tscn").instance()
  Root.add_child(level_select_menu)
  Music.play(Music.boss_menu_intro)
