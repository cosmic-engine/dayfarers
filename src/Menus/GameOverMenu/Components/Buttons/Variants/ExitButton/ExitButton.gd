extends Button

### Terminates the game from the game over menu.

## Quits the game on click.
func _on_button_up():
  get_tree().quit()
