extends Control

### UI screen that appears when the player completes a battle.

var WinMenu = preload("res://Menus/GameOverMenu/BattleVictoryScreen.tscn") # PackedScene
var LossMenu = preload("res://Menus/GameOverMenu/BattleLossScreen.tscn") # PackedScene

## Whether the battle ended in victory.
var _isWin # Boolean

## Constructor.
func init(isWin): # Boolean
  _isWin = isWin

## Load correct menu on ready.
func _ready():
  print("Here")
  var endMenu
  if _isWin:
    endMenu = WinMenu.instance()
  else:
    endMenu = LossMenu.instance()
  add_child(endMenu)
