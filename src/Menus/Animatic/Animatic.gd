extends Node2D

### Scene that contains video player for the opening animatic.

var MainMenu = preload("res://Menus/MainMenu/MainMenu.tscn") # PackedScene
onready var Music = get_tree().get_root().get_node("Music") # Node

## Video player object playing the animatic clip.
onready var video_player = $VideoPlayer

## Pauses music on ready.
func _ready():
  Music.pause(true)

## Listens for input events, skips cutscene if space is pressed.
func _input(event): # Event
  if event.is_action_released("target_confirm"):
    video_player.stop()
    _on_VideoPlayer_finished()

## Switch to main menu scene.
func _load_main_menu():
  Music.pause(false)
  var main_menu = MainMenu.instance()
  get_tree().get_root().call_deferred("add_child", main_menu)

## Loads the main menu when the video finishes.
func _on_VideoPlayer_finished():
  _load_main_menu()
  queue_free()
