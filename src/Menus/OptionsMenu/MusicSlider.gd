extends HSlider


### Set the Volume Level for an Audio Bus
# Attached to a UI component
# Used in the `OptionsMenu` for the Music Volume
# See Related Scripts: `BattleSFXSlider`, `UISlider`, `MusicToggle`


## Name of the Bus
# This one is permanently set to "Music"
export var audio_bus_name := "Music" # String

## Which bus we adjust the volume of
onready var _bus := AudioServer.get_bus_index(audio_bus_name) # AudioServer

## For immediately testing the audio level
# Use this sound effect
onready var SFX = get_node("Sound Effect") # Node

## We also talk to the Music Singleton and adjust its volume where appropriate
onready var Music = get_tree().get_root().get_node("Music") # Node

## We also talk to the Master Volume Control Singleton and adjust its volume where appropriate
onready var MasterVolumeControl = get_tree().get_root().get_node("MasterVolumeControl") # Node

## Configure the Audio bus volume immediately on loading
func _ready() -> void:
  AudioServer.set_bus_volume_db(_bus, linear2db(MasterVolumeControl.music_volume))
  value = MasterVolumeControl.music_volume


## Adjust the Bus volume and test the new volume level each time
# (Connected to a UI element in scene, specific to each menu)
func _on_value_changed(value: float) -> void: # Float
  SFX.play()
  AudioServer.set_bus_volume_db(_bus, linear2db(value))
  MasterVolumeControl.music_volume = value


## Disable volume adjustment if Music is turned off
# (Connected to a UI element in scene, specific to each menu)
func _on_CheckButton_button_up():
  if Music.is_playing == false:
    set_editable(false)
  else:
    set_editable(true) # Replace with function body.
