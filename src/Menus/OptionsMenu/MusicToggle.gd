extends CheckButton


### Toggles Music On and Off
# Attached to a UI component
# Used in the `OptionMenu`
# See Related Scripts: `MusicSlider`, `BattleSFXSlider`, `UISlider`


## Name of the Bus
# This one is permanently set to "Master"
export var audio_bus_name := "Master" # String

## Which bus we adjust the volume of
onready var _bus := AudioServer.get_bus_index(audio_bus_name) # AudioServer

## For immediately testing the audio level
# Use this sound effect
onready var SFX = get_node("Sound Effect") # Node

## We also talk to the Music Singleton and adjust its volume where appropriate
onready var Music = get_tree().get_root().get_node("Music") # Node

## For re-enabling music
# remember what it was last set to before being deactivated
var _volume_value # Int


## Configure the Audio bus volume immediately on loading
func _ready():
  set_pressed(Music.is_playing)
#  _volume_value = Music.volume
  if pressed == false:
    AudioServer.set_bus_mute(_bus, true)
  else: 
    AudioServer.set_bus_mute(_bus, false)


## Adjust the recorded volume each time
# (Connected to a UI element in scene, specific to each menu)
func _on_MusicHSlider_value_changed(value): # Int
  _volume_value = value


## Test the audio level, and mute/unmute the audio
func _on_CheckButton_button_up():
  SFX.play()
  Music.is_playing = pressed
  if pressed == false:
    AudioServer.set_bus_mute(_bus, true)
  else: 
    AudioServer.set_bus_mute(_bus, false)

