extends Button

### Closes the options menu.

## Reference to the reset game message banner UI element.
onready var reset_game_message = $".." # Node

## Reference to the sound effect node.
onready var SFX = get_node("Sound Effect") # Node

## Hides the reset game message on click.
func _on_button_up():
  SFX.play()
  reset_game_message.visible = false
