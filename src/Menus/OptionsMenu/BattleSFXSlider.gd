extends HSlider


### Set the Volume Level for an Audio Bus
# Attached to a UI component
# Used in the `OptionsMenu` for the Battle Sounds Volume
# See Related Scripts: `MusicSlider`, `UISlider`, `MusicToggle`


## Name of the Bus
# This one is permanently set to "Effects"
export var audio_bus_name := "Effects" # String

## Which bus we adjust the volume of
onready var _bus := AudioServer.get_bus_index(audio_bus_name) # AudioServer


## For immediately testing the audio level
# Use this sound effect
onready var SFX = get_node("Sound Effect") # Node

# Note that there is no sound effect for immediately testing the audio level


## We also talk to the Master Volume Control Singleton and adjust its volume where appropriate
onready var MasterVolumeControl = get_tree().get_root().get_node("MasterVolumeControl") # Node


## Configure the Audio bus volume immediately on loading
func _ready() -> void:
  AudioServer.set_bus_volume_db(_bus, linear2db(MasterVolumeControl.effects_volume))
  value = MasterVolumeControl.effects_volume


## Disable volume adjustment if Battle Sounds are turned off
# (Connected to a UI element in scene, specific to each menu)
func _on_CheckButton_toggled(button_pressed):
  set_editable(button_pressed)


## Adjust the Bus volume and DOES NOT test the new volume level each time
# (Connected to a UI element in scene, specific to each menu)
func _on_BattleSFXSlider_value_changed(value) -> void: # Float
  SFX.play()
  AudioServer.set_bus_volume_db(_bus, linear2db(value))
  MasterVolumeControl.effects_volume = value
