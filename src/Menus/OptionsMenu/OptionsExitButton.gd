extends Button

### Button that exits the options menu.

## Reference to the options menu node.
export (NodePath) var optionsMenu # NodePath

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Hide the options menu on button click.
func _on_button_up():
  SFX.play()
  var _op = get_node(optionsMenu)
  _op.set_visible(false)
  #get_node("/root/OptionsMenu").queue_free()
