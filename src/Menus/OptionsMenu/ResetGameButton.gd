extends Button

### Clears game progress when clicked.

## Reference to the game singleton.
onready var Game = get_tree().get_root().get_node("Game") # Node

## Reference to the reset game node.
onready var reset_game_message = $"../../ResetGameMessage" # Node

## Reference to the SFX player node.
onready var SFX = get_node("Sound Effect") # Node

## Clears progress flags for the game.
func _on_button_up():
  SFX.play()
  Game.victories["liar"] = false
  Game.victories["cheater"] = false
  Game.victories["thief"] = false
  reset_game_message.visible = true
