import os
import re
import sys
import subprocess
import time

class Class:
    
    def __init__(self, name):
        self.name = name
        self.parent = "Node"
        self.description = ""
        self.members = {} # String (var name) -> String (var type)
        self.memberDescriptions = {} # String (var name) -> String (var description)
        self.signals = {} # String (sig name) -> {String (param name) -> String (param type)}
        self.functions = {} # String (func name) -> {String (param name) -> String (param type)}
        # function return types are stored in self.functions[function]["RETURNTYPE"]
        # function and signal descriptions are stored in self.functions/self.signals[function/signal]["DESCRIPTION"]

    def __str__(self):
        res = "Name: " + self.name + "\n"
        res += "Extends: " + self.parent + "\n"
        res += "Description: " + self.description + "\n"
        res += "Members: " + str(self.members) + "\n"
        res += "Member Descriptions: " + str(self.memberDescriptions) + "\n"
        res += "Signals: " + str(self.signals) + "\n"
        res += "Functions: " + str(self.functions)
        return res


def writeClassToTex(o):
    res = "\\subsection {" + o.name + "}\n"
    res += "\\label{" + o.name + "}\n"
    res += "\t" + o.description + "\n"
    res += "\t" + r"\begin{center}" + "\n"
    res += "\t\t" + r"\begin{figure}[H]" + "\n"
    res += "\t\t\t\\centering\n"
    res += "\t\t\t\\includegraphics[width=6cm]{Diagrams/ClassDiagrams/" + o.name + "ClassDiagram.png}\n"
    res += "\t\t\t\\caption{" + o.name + " Class Diagram}\n"
    res += "\t\t" + r"\end{figure}" + "\n"
    res += "\t\t" + r"\begin{longtable}{|c|c|p{8cm}|}" + "\n"
    res += "\t\t\t\\hline\n"
    res += "\t\t\tName & Access & Description\\\\\n"

    for member in o.members:
        res += "\t\t\t\\hline\n"
        res += "\t\t\t" + member + " & "
        if member.startswith("_"):
            res += "private & "
        else:
            res += "public & "
        res += o.memberDescriptions[member] +  " \\\\\n"

    for signal in o.signals:
        res += "\t\t\t\\hline\n"
        res += "\t\t\t" + signal + " & "
        if signal.startswith("_"):
            res += "private & "
        else:
            res += "public & "

        description = o.signals[signal]["DESCRIPTION"]
        res += description + " \\\\\n"
    
    res += "\t\t\t\\hline\n"
    res += "\t\t\t" + r"\caption{Attributes}" + "\n"
    res += "\t\t" + r"\end{longtable}" + "\n"
    res += "\t" + r"\end{center}" + "\n"

    res += "\t" + r"\begin{center}" + "\n"
    res += "\t\t" + r"\begin{longtable}[t]{|p{4cm}|p{3cm}|p{2cm}|p{4cm}|}" + "\n"
    res += "\t\t\t\\hline\n"
    res += "\t\t\tName & Input & Output & Description \\\\\n"

    for function in o.functions:
        res += "\t\t\t\\hline\n"
        res += "\t\t\t" + str(function).strip() + " & "
        paramInfo = o.functions[function]
        returntype = paramInfo["RETURNTYPE"]
        description = paramInfo["DESCRIPTION"]

        if len(paramInfo.keys()) == 1:
            res += "void"
        else:
            for param in paramInfo:
                if param != "RETURNTYPE" and param != "DESCRIPTION":
                    res += str(param).strip() + ", "
            res = res[:-2]

        res += " & " + returntype + " & " + description + " \\\\\n"

    res += "\t\t\t" + r"\hline" + "\n"
    res += "\t\t\t" + r"\caption{Methods}" + "\n"
    res += "\t\t" + r"\end{longtable}" + "\n"
    res += "\t" + r"\end{center}" + "\n"

    print(res)
    return res


def writeClassToMermaid(o):
    #res = o.name + "\n"

    filename = "./MermaidDiagrams/" + o.name + "ClassDiagram.mmd"

    f = open(filename, "w")

    res = "classDiagram\n"
    res += "\tclass " + o.name + "{\n"
    for member in o.members:
        if member.startswith("_"):
            res += "\t\t-"
        else:
            res += "\t\t+"
        res += o.members[member] + " " + member + "\n"

    for signal in o.signals:
        if signal.startswith("_"):
            res += "\t\t-"
        else:
            res += "\t\t+"
        res += "signal " + signal + "\n"

    for function in o.functions:
        if function.startswith("_"):
            res += "\t\t-"
        else:
            res += "\t\t+"
        res += function + "("

        paramInfo = o.functions[function]
        returntype = paramInfo["RETURNTYPE"]
        if len(paramInfo) == 1:
            res += ")"
        else:
            for param in paramInfo:
                if param != "RETURNTYPE":
                    res += paramInfo[param] + ","
            res = res[:-1]
            res += ")"
        
        if returntype != "void":
            res += " " + returntype + "\n"
        else:
            res += "\n"

    res += "\t}\n"

    f.write(res)
    f.close

    print(res)
    return res

def createMermaidPNG(o):
    filename = "./MermaidDiagrams/" + o.name + "ClassDiagram.mmd"
    outputfilename = "./MermaidDiagrams/Images/" + o.name + "ClassDiagram.png"
    command = "mmdc -i " + filename + " -o " + outputfilename + " -t dark -b transparent"
    print("Generating pcture for " + outputfilename)
    os.system(command)

def recurse(dir):
    count = 0

    objs = []

    # any file under a directory "test" will NOT be parsed!!!
    exclude = set(["test"])

    for root, subdirs, files in os.walk(dir):
        subdirs[:] = [d for d in subdirs if d not in exclude]

        for filename in files:
            if (filename.endswith(".gd")):
                hasGd = True
                break

        if hasGd:
            for filename in files:
                if (filename.endswith(".gd")):
                    objs.append(parseFile(os.path.join(root, filename)))
                    count += 1

        for d in subdirs:
            objs += recurse(d)
    
    return objs

def parseFile(filepath):
    f = open(filepath, "r")

    name = f.name.split(os.path.sep)[-1].removesuffix(".gd")
    obj = Class(name)

    section = "Members"
    describing = False
    multiLineFuncParams = False
    parameterTypes = None
    returnVal = ""
    classDescribing = False
    classDescription = ""
    while True:
        line = f.readline()
        if not line:
            obj.description = classDescription
            break
        if describing == False:
            description = ""
        if multiLineFuncParams == False:
            multiLineFuncParameters = []

        if line.startswith("extends"):
            obj.parent = line.split(" ")[1].removesuffix("\n")

        elif line.startswith("###"):
            classDescription = line.removeprefix("###").strip().removesuffix("\n")
            classDescribing = True

        elif line.startswith("##"):
            description = line.removeprefix("##").strip().removesuffix("\n")
            describing = True

        elif classDescribing and line.startswith("#"):
            classDescription += " " + line.removeprefix("#").removeprefix("#").removeprefix("#").strip().removesuffix("\n")

        elif describing and line.startswith("#"):
            description += " " + line.removeprefix("#").removeprefix("#").strip().removesuffix("\n")

        elif line.startswith("export var") and section == "Members":
            rest = line.removeprefix("export var ")
            name = rest.split(":")[0].replace(" ", "").removesuffix("\n")
            name = name.split("=")[0].replace(" ", "").removesuffix("\n")
            if "#" in line:
                vartype = line.split("#")[1].replace(" ", "").removesuffix("\n")
            else:
                vartype = "None"
            obj.members[name] = vartype
            obj.memberDescriptions[name] = description
            describing = False
            classDescribing = False
        
        elif line.startswith("export(") and section == "Members":
            # rest = re.sub(r" ?\([^)]+\)", "", line)
            # rest = rest.removeprefix("export var ")
            rest = line.split("var")[1].split("#")[0].split("=")[0].strip()
            name = rest.split(":")[0].replace(" ", "").removesuffix("\n")
            name = name.split("=")[0].replace(" ", "").removesuffix("\n")
            if "#" in line:
                vartype = line.split("#")[1].replace(" ", "").removesuffix("\n")
            else:
                vartype = "None"
            obj.members[name] = vartype
            obj.memberDescriptions[name] = description
            describing = False
            classDescribing = False

        elif line.startswith("enum") and section == "Members":
            rest = line.removeprefix("enum ")
            name = rest.split(":")[0].replace(" ", "").removesuffix("\n")
            name = name.split("=")[0].replace(" ", "").removesuffix("\n")
            name = name.split("{")[0].replace(" ", "").removesuffix("\n")
            if "#" in line:
                vartype = line.split("#")[1].replace(" ", "").removesuffix("\n")
            else:
                vartype = "None"
            obj.members[name] = vartype
            obj.memberDescriptions[name] = description
            describing = False
            classDescribing = False

        elif line.startswith("onready var") and section == "Members":
            if "preload" in line:
                continue
            rest = line.removeprefix("onready var ")
            name = rest.split(":")[0].replace(" ", "").removesuffix("\n")
            name = name.split("=")[0].replace(" ", "").removesuffix("\n")
            if "#" in line:
                vartype = line.split("#")[1].replace(" ", "").removesuffix("\n")
            else:
                vartype = "None"
            obj.members[name] = vartype
            obj.memberDescriptions[name] = description
            describing = False
            classDescribing = False

        elif line.startswith("var") and section == "Members":
            if "preload" in line:
                continue
            rest = line.removeprefix("var ")
            #name = rest.split(":")[0].replace(" ", "").removesuffix("\n")
            name = rest.split(" ")[0].split("=")[0].removesuffix("\n")
            if "#" in line:
                vartype = line.split("#")[1].replace(" ", "").removesuffix("\n")
            else:
                vartype = "None"
            obj.members[name] = vartype
            obj.memberDescriptions[name] = description
            describing = False
            classDescribing = False

        elif line.startswith("signal"):
            name = line.removeprefix("signal ").split("(")[0].removesuffix(")\n")
            parameters = line.split("(")[1].split(")")[0].replace(" ", "")
            parameters = parameters.split(",")

            if "#" in line:
                parameterTypes = line.split("#")[1].replace(" ", "").removesuffix("\n")
                parameterTypes = parameterTypes.split(",")
            else:
                parameterTypes = []

            if len(parameters) == len(parameterTypes):
                paramPairs = {}
                for i in range(len(parameters)):
                    paramPairs[parameters[i]] = parameterTypes[i]
            else:
                paramPairs = {}
            obj.signals[name] = paramPairs
            obj.signals[name]["DESCRIPTION"] = description
            describing = False
            classDescribing = False

        elif line.startswith("func"):
            section = "Functions"
            name = line.split("(")[0].removeprefix("func").strip()

            parameterTypes = None
            returns = "void"
            returnVal = returns
            if "#" in line:
                parameterTypes = []
                annotation = line.split("#")[1].replace(" ", "").removesuffix("\n").removesuffix(",")
                if "->" in annotation:
                    returns = annotation.split("->")[1].replace(" ", "").removesuffix("\n")
                    returnVal = returns
                    parameterTypes = annotation.split("->")[0]
                    if parameterTypes == "":
                        parameterTypes = []
                    else:
                        parameterTypes = parameterTypes.split(",")
                else:
                    parameterTypes = annotation.split(",")

            if "):" in line:
                parameters = line.split("(")[1].split("):")[0].removesuffix("\n")
                parameters.replace(" ", "")
                if parameters == "":
                    parameters = None
                else:
                    parameters = parameters.split(",")
            else:
                multiLineFuncParams = True
                continue

            paramPairs = {}
            if parameters == None or parameterTypes == None:
                pass
            elif len(parameters) == len(parameterTypes):
                for i in range(len(parameters)):
                    paramPairs[parameters[i]] = parameterTypes[i]

            paramPairs["RETURNTYPE"] = returns
            paramPairs["DESCRIPTION"] = description
            describing = False
            classDescribing = False
            obj.functions[name] = paramPairs

        elif multiLineFuncParams == True:
            if line.strip().removesuffix("\n") == "):":
                multiLineFuncParams = False
            elif line.removesuffix("\n").endswith("):"):
                multiLineFuncParameters.append(line.removesuffix("\n").strip().removesuffix("):").removesuffix(","))
                multiLineFuncParams = False
            else:
                multiLineFuncParameters.append(line.removesuffix(",\n").strip())
                continue

            paramPairs = {}
            if multiLineFuncParameters == None or parameterTypes == None:
                pass
            elif len(multiLineFuncParameters) == len(parameterTypes):
                for i in range(len(multiLineFuncParameters)):
                    paramPairs[multiLineFuncParameters[i]] = parameterTypes[i]

            paramPairs["RETURNTYPE"] = returnVal
            paramPairs["DESCRIPTION"] = description
            describing = False
            classDescribing = False
            obj.functions[name] = paramPairs
            multiLineFuncParams = False

        if classDescribing == False:
            obj.description = classDescription

    f.close()
    return obj

path = os.path.join(os.getcwd(), "src")
classarray = recurse(path)
classarray.sort(key=lambda x: x.name, reverse=False)


f = open("parseoutput.txt", "a")
f.truncate(0)

for c in classarray:
    #print(c)
    output = writeClassToTex(c)
    f.write(output)
    #writeClassToMermaid(c)

#for c in classarray:
#    createMermaidPNG(c)

f.close()
print(str(len(classarray)) + " files parsed.")
print("Outputs written to parseToTex.txt and parseToImg.txt")

#path = r"E:\Documents\GitHub\dayfarers-spring\src\Battle\Arenas\Arena\Arena.gd"
#parseFile(path)
